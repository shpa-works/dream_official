<?php
//ライブラリ読み込み
require "../inc/option.inc";

$siteTitle = 'WEBクーポン｜軽自動車専門店・ドリームのWEBクーポンお問い合わせ';
$pageTitle = 'WEBクーポン お問い合わせ';
$metaDescription = '加古川・福知山・舞鶴での軽自動車専門店ドリームのWEBクーポンをメールで配布しております。各メーカーの最新軽自動車を常時1000台以上在庫しております。加古川・福知山・舞鶴で軽自動車を買うなら地域最大級のドリームにお越しください。';

//レイアウト読み込み
require incPATH."/layout.inc";

?><!DOCTYPE html>
<html lang="ja"><head>
<meta charset="UTF-8">
<title><?= $siteTitle; ?></title>
<?= $meta; ?>
<?= $css; ?>
<?= $js; ?>

</head>

<body id="contact" class="thanks coupon">
<?= $noscript; ?>
<div id="keyVisual">
	<h2 class="page_title">
		<span class="jp"><?= $pageTitle; ?></span><br>
		<span class="en">COUPON</span>
	</h2>
</div>

<ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="../"><span itemprop="name">トップ</span></a>
    <meta itemprop="position" content="1">
  </li>
  <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
    <span itemprop="name">Webクーポン お問い合わせ</span>
    <meta itemprop="position" content="2">
  </li>
</ol>
<div id="wrapper">
<?= $header; ?>
<?= $nav; ?>


<article>
<section>
<h3 class="con_ttl">お問い合わせ完了</h3>
  <div class="txt_box thanks">
    <p>WEBクーポンお問い合わせありがとうございました。<br />自動返信メールが送信されます。<br class="pc_none" />内容をご確認下さい。</p>
    <p>メールが届かない場合は以下の可能性がありますのでご確認下さい。</p>
    <ul class="disc">
      <li>メールアドレスの入力ミスの可能性（全角・半角の違い／大文字・小文字の違い）</li>
      <li>フリーメール(Yahoo・Hotmail・Goo)やメーラーの関係で「迷惑フォルダ」に移動されている可能性<br>（念のためご確認をお願いいたします）</li>
    </ul>
  </div>
</section>
</article>

</div><!-- /#wrapper -->

<section class="footer_bnr">
  <div class="wrapper">
    <div class="holder clearfix">
      <div class="bnr"><a target="_blank" href="https://shaken.dreamjapan.jp/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn01.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://dreamjapan.jp/kakogawa/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn02.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://dreamjapan.jp/hayataro/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn03.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://bankin.dreamjapan.jp/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn04.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://bankin.dreamjapan.jp/kakogawa/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn05.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://www.j-netrentacar.co.jp/kyoto/maizuru/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn06.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://kyoto-brc.com/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn07.png" alt=""></a></div>
    </div>
  </div>
</section>

<section class="footer_info">
  <div class="wrapper">
    <div class="holder clearfix">
      <div class="box height">
        <p class="title"><span>ドリーム加古川本店</span></p>
        <div class="phone">
          <a href="">079-434-3000</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒675-0053<br>兵庫県加古川市⽶⽥町船頭字⾕107-3</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>第3水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream_kakogawa?share" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリーム福知⼭店</span></p>
        <div class="phone">
          <a href="">0773-23-0031</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒620-0000<br>京都府福知山市篠尾961−44</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream-fukushiyama?share" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリーム プレミアム</span></p>
        <div class="phone">
          <a href="">0773-78-3399</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒624-0946<br>京都府舞鶴市下福井51</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream_maizuru?share" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>軽アウトレット専⾨店</span></p>
        <div class="phone">
          <a href="">0773-25-0398</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒620-0061<br>京都府福知⼭市荒河東町100</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/keidream-outlet?share "target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリームMEGA熊本店</span></p>
        <div class="phone">
          <a href="">096-349-0100</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒861-8035<br>熊本県熊本市東区御領8丁目10-101</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://goo.gl/maps/sFt6iTTRJG5QhPeG8" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>
    </div>
  </div>
</section>

<footer>
  <div class="footer_cont clearfix">
    <div class="footer_logo"><a href="https://dreamjapan.jp"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_logo.png" alt="ドリームジャパン"></a></div>
    <div class="nav">
      <ul class="bloc">
        <li><a href="https://dreamjapan.jp/dream/">ドリームの魅力</a></li>
        <li><a href="https://dreamjapan.jp/news/">新着情報</a></li>
        <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
      </ul>
      <ul class="bloc">
        <li>
          <a href="https://dreamjapan.jp/service/">サービス</a>
          <ul class="clearfix">
            <li><a href="https://dreamjapan.jp/service/unused/">未使用車販売</a></li>
            <li><a href="https://dreamjapan.jp/service/maintenance/">車検・整備</a></li>
            <li><a href="https://dreamjapan.jp/service/repair/">鈑金・修理</a></li>
            <li><a href="https://dreamjapan.jp/service/insurance/">保険</a></li>
            <li><a href="https://dreamjapan.jp/service/carrental/">レンタカー</a></li>
            <li><a href="https://dreamjapan.jp/service/flatseven/">フラット7</a></li>
            <li><a href="https://k-398.dreamjapan.jp/" target="_blank">中古車販売39.8</a></li>
          </ul>
        </li>
      </ul>
      <ul class="bloc">
        <li><a href="https://dreamjapan.jp/information/">店舗紹介</a></li>
        <li><a href="https://dreamjapan.jp/news/">スタッフブログ</a></li>
      </ul>
      <ul class="bloc">
        <li><a href="https://dreamjapan.jp/about/">会社案内</a></li>
        <li><a href="https://dreamjapan.jp/contact/">お問い合わせ</a></li>
        <li><a href="https://dreamjapan.jp/privacypolicy/">プライバシーポリシー</a></li>
      </ul>
    </div>
    <ul class="nav_sp">
      <li><a href="https://dreamjapan.jp/dream/">ドリームの魅力</a></li>
      <li><a href="https://dreamjapan.jp/service/">サービス</a></li>
      <li><a href="https://dreamjapan.jp/news/">新着情報</a></li>
      <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
      <li><a href="https://dreamjapan.jp/information/">店舗紹介</a></li>
      <li><a href="https://dreamjapan.jp/info/">スタッフブログ</a></li>
      <li><a href="https://dreamjapan.jp/information">会社案内</a></li>
      <li><a href="https://dreamjapan.jp/privacypolicy/">プライバシーポリシー</a></li>
    </ul>
    <div class="contact_btn sp-on">
      <a href="https://dreamjapan.jp/contact/"><span>お問い合わせ</span></a>
    </div>
  </div>
  <div class="copyright"><p>©2022 Dream Japan.co,.ltd All Rights Reserved.</p></div>
</footer>

<!--<div id="page-top"><a href="#"><img src="/lib/cmn-img/common/pagetop.jpg"></a></div>-->
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-js/all.js"></script>
<script src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-js/parts.js"></script>
</body>
</html>
