<?php
$siteTitle = 'WEBクーポン｜軽自動車専門店・ドリームのWEBクーポンお問い合わせ';
$pageTitle = 'WEBクーポン お問い合わせ';
$metaDescription = '加古川・福知山・舞鶴での軽自動車専門店ドリームのWEBクーポンをメールで配布しております。各メーカーの最新軽自動車を常時1000台以上在庫しております。加古川・福知山・舞鶴で軽自動車を買うなら地域最大級のドリームにお越しください。';


//ライブラリ読み込み
require "../inc/option.inc";
require "../inc/pref.inc";
require "../inc/library.inc";
require "../inc/mod_dateselectgenerator.php";

// セッションのスタート
session_start();

$tenpo = array("加古川本店","福知山店","舞鶴店");

$file = basename($_SERVER['PHP_SELF']);
$errormsg = '';

if (!empty($_POST)) {
	// 全角文字を半角に変換
	$_FORM['tel'] = mb_convert_kana($_FORM['tel'],'rn');
	$_FORM['email'] = mb_convert_kana($_FORM['email'],'rn');

	if((!empty($_POST['confirm']) || !empty($_POST['zaiko']))){
	  $_SESSION = $_FORM;
	}

	// PHPでチェック
	$msg = "";
	$e_flag = 0;
  if($_SESSION['name'] == ""){
      $msg .= "お名前が入力されていません。<br />";
      $e_flag = 1;
  }
  if($_SESSION['tel'] == ""){
      $msg .= "電話番号が入力されていません。<br />";
      $e_flag = 1;
  }
	if($_SESSION['email'] == ""){
      $msg .= "メールアドレスが入力されていません。<br />";
      $e_flag = 1;
	}/*
	if($_SESSION['tel'] == "" && $_SESSION['email'] == ""){
      $msg .= "ご連絡先が入力されていません。<br />電話番号かメールアドレスをご入力下さい。<br />";
      $e_flag = 1;
	}*/
	if($e_flag == 1){ $errormsg = '<p class="center" style="color:#F00">'.$msg."</p>"; }
}

if(!empty($_POST['submit']) and $e_flag == 0){
    // 設定読み込み
    include incPATH."/mail.inc";
    // メッセージ生成
    $MsgData = <<< EOD
お名前：{$_SESSION['name']}
メールアドレス：{$_SESSION['email']}
電話番号：{$_SESSION['tel']}
最寄りの店舗：{$_SESSION['tenpo']}
EOD;

    $msg = str_replace("%MsgData%", $MsgData, $couponMsg);

    mb_language("uni") ;
    mb_internal_encoding("UTF-8") ;

    $msg = html_entity_decode($msg);
    $title = html_entity_decode($couponSubject);
    $from = mb_encode_mimeheader(html_entity_decode($couponFromName)).' <'.$couponFromEmail.'>';

    mb_send_mail($_SESSION['email'], $title, $msg, "From:".$from."\n", "-f{$couponFromEmail}");
	if(strpos($_SESSION['email'], '@tratto-brain.jp') !== FALSE) {
      mb_send_mail($_SESSION['email'], "お申込み受付ました", $msg, "From:".$from."\n", "-f{$couponFromEmail}");
    } else {
      mb_send_mail($couponToEmail, "お申込み受付ました", $msg, "From:".$from."\n", "-f{$couponFromEmail}");
	}

    $fp = fopen("log.csv","a");
    fwrite($fp, '"'.date("Y-m-d H:i").'","'.mb_convert_encoding($MsgData,"SJIS").'"');
    fwrite($fp,"\n");
    fclose($fp);

    $_SESSION = array();

   header("Location: thanks.php");
}

//レイアウト読み込み
require incPATH."/layout.inc";

?><!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title><?= $siteTitle; ?></title>
<?= $meta; ?>
<?= $css; ?>
<link rel="stylesheet" href="../js/jquery.validate.css" type="text/css" media="all" />
<link type="text/css" href="../css/no-theme/jquery-ui-1.7.1.custom.css" rel="stylesheet" />

<?= $js; ?>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/messages_ja.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script type="text/javascript">
$(function(){
  $("#myForm").validate();
});
</script>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
</head>

<body id="contact" class="coupon">
<?= $noscript; ?>

<div id="keyVisual">
	<h2 class="page_title">
		<span class="jp"><?= $pageTitle; ?></span><br>
		<span class="en">COUPON</span>
	</h2>
</div>

<ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
  <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
    <a itemprop="item" href="../"><span itemprop="name">トップ</span></a>
    <meta itemprop="position" content="1">
  </li>
  <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
    <span itemprop="name">Webクーポン お問い合わせ</span>
    <meta itemprop="position" content="2">
  </li>
</ol>
<div id="wrapper">
<?= $header; ?>
<?= $nav; ?>

<article>

<form action="<?php echo $file; ?>" method="post" id="myForm" name="myForm">

<?php
if ($e_flag==0 && (!empty($_POST['confirm']) || !empty($_POST['zaiko']))) {
//確認画面
?>

<section>
  <h3 class="con_ttl">ご入力内容確認</h3>
  <div class="center">下記の確認内容でよろしければ、送信ボタンを押して下さい。</div>
  <!-- 確認start -->
  <dl class="dl_box flex">

    <dt>お名前 <span class="red small">（※）</span></dt>
    <dd><?php echo $_SESSION['name']; ?></dd>

    <dt>Eメール <span class="red small">（※）</span></dt>
    <dd><?php echo $_SESSION['email']; ?></dd>

    <dt>電話番号 <span class="red small">（※）</span></dt>
    <dd><?php echo $_SESSION['tel']; ?></dd>

    <dt>最寄りの店舗</dt>
    <dd><?php echo $_SESSION['tenpo']; ?></dd>

  </dl>

  <div class="center con submit-btn submit-btn-submit">
    <input type="submit" name="submit" value=" 送信 " />　<input type="button" value=" 戻る " onClick="location.href='<?php echo $file; ?>'" />
  </div>
</section>

<?php
}else{
//入力フォーム
?>

<section id="coupon">
<p class="bn_webtokuten"><img src="form_coupon.jpg" alt="WEB限定クーポン" class="wide"></p>
</section>

<section>
  <h3 class="con_ttl">ご成約時に使えるお得なクーポンをプレゼント</h3>
    <div class="center">
    	<p class="txt">ご入力頂いたメールアドレスにクーポンをメールで配布しています。<br>
			お名前とメールアドレスを入力後、送信ボタンを押してください。</p>
    </div>

<?php echo $errormsg; ?>

<!-- 入力start -->
  <dl class="dl_box flex">
    <dt>お名前 <span class="red small">（※）</span></dt>
    <dd><input type="text" name="name" value="<?php echo $_SESSION['name']; ?>" class="required" /></dd>

    <dt>Eメール <span class="red small">（※）</span></dt>
    <dd><input type="text" name="email" id="email" value="<?php echo $_SESSION['email']; ?>" class="email required" /></dd>

    <dt>電話番号 <span class="red small">（※）</span></dt>
    <dd><input type="text" name="tel" value="<?php echo $_SESSION['tel']; ?>" class="required" /></dd>

    <dt>最寄りの店舗</dt>
    <dd><?= create_radio1("tenpo", $tenpo, $_SESSION['tenpo'], "加古川本店", ""); ?></dd>

  </dl>

  <div class="center con submit-btn submit-btn-confirm">
    <input type="submit" name="confirm" value=" この内容で確認する " />
  </div>
<?php } ?>
  </section>
  </form>
</article>

<?php //echo $footer; ?>
</div><!-- /#wrapper -->
<!-- 一時　WPのフッター貼り付け -->
<section class="footer_bnr">
  <div class="wrapper">
    <div class="holder clearfix">
      <div class="bnr"><a target="_blank" href="https://shaken.dreamjapan.jp/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn01.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://dreamjapan.jp/kakogawa/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn02.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://dreamjapan.jp/hayataro/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn03.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://bankin.dreamjapan.jp/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn04.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://bankin.dreamjapan.jp/kakogawa/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn05.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://www.j-netrentacar.co.jp/kyoto/maizuru/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn06.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://kyoto-brc.com/"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_btn07.png" alt=""></a></div>
    </div>
  </div>
</section>

<section class="footer_info">
  <div class="wrapper">
    <div class="holder clearfix">
      <div class="box height">
        <p class="title"><span>ドリーム加古川本店</span></p>
        <div class="phone">
          <a href="">079-434-3000</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒675-0053<br>兵庫県加古川市⽶⽥町船頭字⾕107-3</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>第3水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream_kakogawa?share" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリーム福知⼭店</span></p>
        <div class="phone">
          <a href="">0773-23-0031</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒620-0000<br>京都府福知山市篠尾961−44</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream-fukushiyama?share" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリーム プレミアム</span></p>
        <div class="phone">
          <a href="">0773-78-3399</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒624-0946<br>京都府舞鶴市下福井51</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream_maizuru?share" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>軽アウトレット専⾨店</span></p>
        <div class="phone">
          <a href="">0773-25-0398</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒620-0061<br>京都府福知⼭市荒河東町100</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/keidream-outlet?share "target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリームMEGA熊本店</span></p>
        <div class="phone">
          <a href="">096-349-0100</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒861-8035<br>熊本県熊本市東区御領8丁目10-101</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://goo.gl/maps/sFt6iTTRJG5QhPeG8" target="_blank"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>
    </div>
  </div>
</section>

<footer>
  <div class="footer_cont clearfix">
    <div class="footer_logo"><a href="https://dreamjapan.jp"><img src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-img/common/footer_logo.png" alt="ドリームジャパン"></a></div>
    <div class="nav">
      <ul class="bloc">
        <li><a href="https://dreamjapan.jp/dream/">ドリームの魅力</a></li>
        <li><a href="https://dreamjapan.jp/news/">新着情報</a></li>
        <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
      </ul>
      <ul class="bloc">
        <li>
          <a href="https://dreamjapan.jp/service/">サービス</a>
          <ul class="clearfix">
            <li><a href="https://dreamjapan.jp/service/unused/">未使用車販売</a></li>
            <li><a href="https://dreamjapan.jp/service/maintenance/">車検・整備</a></li>
            <li><a href="https://dreamjapan.jp/service/repair/">鈑金・修理</a></li>
            <li><a href="https://dreamjapan.jp/service/insurance/">保険</a></li>
            <li><a href="https://dreamjapan.jp/service/carrental/">レンタカー</a></li>
            <li><a href="https://dreamjapan.jp/service/flatseven/">フラット7</a></li>
            <li><a href="https://k-398.dreamjapan.jp/" target="_blank">中古車販売39.8</a></li>
          </ul>
        </li>
      </ul>
      <ul class="bloc">
        <li><a href="https://dreamjapan.jp/information/">店舗紹介</a></li>
        <li><a href="https://dreamjapan.jp/news/">スタッフブログ</a></li>
      </ul>
      <ul class="bloc">
        <li><a href="https://dreamjapan.jp/about/">会社案内</a></li>
        <li><a href="https://dreamjapan.jp/contact/">お問い合わせ</a></li>
        <li><a href="https://dreamjapan.jp/privacypolicy/">プライバシーポリシー</a></li>
      </ul>
    </div>
    <ul class="nav_sp">
      <li><a href="https://dreamjapan.jp/dream/">ドリームの魅力</a></li>
      <li><a href="https://dreamjapan.jp/service/">サービス</a></li>
      <li><a href="https://dreamjapan.jp/news/">新着情報</a></li>
      <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
      <li><a href="https://dreamjapan.jp/information/">店舗紹介</a></li>
      <li><a href="https://dreamjapan.jp/info/">スタッフブログ</a></li>
      <li><a href="https://dreamjapan.jp/information">会社案内</a></li>
      <li><a href="https://dreamjapan.jp/privacypolicy/">プライバシーポリシー</a></li>
    </ul>
    <div class="contact_btn sp-on">
      <a href="https://dreamjapan.jp/contact/"><span>お問い合わせ</span></a>
    </div>
  </div>
  <div class="copyright"><p>©2022 Dream Japan.co,.ltd All Rights Reserved.</p></div>
</footer>

<!--<div id="page-top"><a href="#"><img src="/lib/cmn-img/common/pagetop.jpg"></a></div>-->
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-js/all.js"></script>
<script src="https://dreamjapan.jp/blog/wp-content/themes/dream_official/lib/cmn-js/parts.js"></script>
<!-- ここまで -->

</body>
</html>
