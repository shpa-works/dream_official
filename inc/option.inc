<?php
session_start();

// フォームデータをFORMに配列
if (!empty($_POST) || !empty($_GET)) {
  $data = !empty($_POST) ? $_POST : $_GET;
  $_FORM = hrtrim( $data );
}

function hrtrim( $data ){
  if( is_array($data) ){
    foreach($data as $key => $value){
      $data[$key] = hrtrim($value);
    }
  }else{
    $data = htmlspecialchars($data, ENT_QUOTES);
  }
  return $data;
}

date_default_timezone_set('Asia/Tokyo');
$now_y = date("Y");
$now_m = date("m");
$now_d = date("d");

define("PATH", "/var/www/vhosts/dreamjapan.jp/httpdocs");
define("incPATH", "/var/www/vhosts/dreamjapan.jp/httpdocs/inc");

$selected = ' selected="selected"';
$checked = ' checked="checked"';

$li_state = array( 0 => '表示', 88 => '非表示' );
$li_cstate = array( 0 => '表示', 11 => 'SOLDOUT', 88 => '非表示' );

//店舗
$_shop['kakogawa'] = '加古川本店';
$_shop['fukuchiyama'] = '福知山店';
$_shop['maizuru'] = '舞鶴店';

//価格帯
$li_price['50'] = "50万円以下";
$li_price['70'] = "70万円以下";
$li_price['90'] = "90万円以下";
$li_price['110'] = "110万円以下";
$li_price['130'] = "130万円以下";
$li_price['150'] = "150万円以下";
$li_price['151'] = "150万円以上";

//チラシ
//$_chirashi['ad_pana_omote'] = 'パナソニックｘドリーム合同野外展示会 表面';
//$_chirashi['ad_pana_ura'] = 'パナソニックｘドリーム合同野外展示会 裏面';
$_chirashi['ad_kako'] = '加古川本店<br>表面';
$_chirashi['ad_kako_ura'] = '加古川本店<br>裏面';
$_chirashi['ad_fuku'] = '福知山店 表面';
$_chirashi['ad_fuku_ura'] = '福知山店 裏面';
$_chirashi['ad_omote'] = '舞鶴店 表面';
$_chirashi['ad_ura'] = '舞鶴店 裏面';
$_chirashi['ad_flat_omote'] = '舞鶴店<br>フラット7 表面';
$_chirashi['ad_flat_ura'] = '舞鶴店<br>フラット7 裏面';
$_chirashi['ad_outlet_omote'] = '福知山店<br>OUTLET 表面';
$_chirashi['ad_outlet_ura'] = '福知山店<br>OUTLET 裏面';

//電話番号
$_shopTel['kako'] = '079-434-3000';
$_shopTel['fuku'] = '0773-23-0031';
$_shopTel['outlet'] = '0773-25-0398';
$_shopTel['maiduru'] = '0773-78-3399';

?>