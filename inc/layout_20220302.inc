<?php
//サイト名
$siteTitle = !empty($siteTitle) ? $siteTitle : 'ドリーム・軽未使用車専門店｜熊本・加古川・福知山・舞鶴最大級1000台在庫';
$metaDescription = !empty($metaDescription) ? $metaDescription : "熊本・加古川・福知山・舞鶴で軽自動車の未使用車・新古車のことなら地域最大級の軽自動車専門店ドリームにお任せください！スズキ・ダイハツ・ホンダ・三菱・日産等の最新軽自動車を常時1000台以上在庫しております。熊本・加古川・福知山・舞鶴で軽自動車を買うならドリームにお越しください。";

$meta = <<< EOD

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T2RND72');</script>
<!-- End Google Tag Manager -->

<meta name="description" content="{$metaDescription}">
<meta name="keywords" content="軽自動車,未使用車,新古車,中古車,熊本,加古川,福知山,舞鶴,兵庫,京都北部,スズキ,ダイハツ,ホンダ,三菱,日産">
<meta name="author" content="ドリームジャパン" />
<meta name="copyright" content="ドリームジャパン"/>
EOD;

$noscript = <<< EOD
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T2RND72"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
EOD;

$_SESSION['sp'] = !empty($_GET['sp']) ? $_GET['sp'] : $_SESSION['sp'];
$ua = 'pc';
$tmp = $_SERVER['HTTP_USER_AGENT'];
if (empty($_SESSION['sp'])) {
  if((strpos($tmp,'iPhone')!==false)||(strpos($tmp,'iPod')!==false)||(strpos($tmp,'Android')!==false) && (strpos($tmp,'Mobile')!==false)) {
    $ua = 'sp';
  }
}

if ($ua == 'pc') {

// css
if ( $_SERVER['PHP_SELF'] != "/index.html") {
  $css = <<< EOD
<link rel="stylesheet" href="/css/hover.css" />
<link rel="stylesheet" href="/css/common.css" />
<link rel="stylesheet" href="/css/base.css?20190315" />
<link rel="stylesheet" href="/css/contents.css?202010" />
EOD;
} else {
  $css = <<< EOD
<link rel="stylesheet" href="/css/common.css" />
<link rel="stylesheet" href="/css/base.css?20190213" />
<link rel="stylesheet" href="/css/top.css?20200722" />
<link rel="stylesheet" href="/css/hover.css" />
EOD;
}

$js = <<< EOD
<link rel="apple-touch-icon" href="/img/sp/icon.png" />
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->
<script src="/js/jquery.js"></script>
<script src="/js/script.js"></script>
\n
EOD;


$prettyPhoto = <<< EOD
<link rel="stylesheet" href="../css/prettyPhoto.css" />
<script src="/js/jquery.prettyPhoto.js"></script>
\n
EOD;

//ヘッダー

$header = <<< EOD
<header class="fw flex between inner">
  <div>
    <h1>熊本で軽自動車ならドリームMEGA熊本店<br>加古川・姫路・明石で軽自動車ならドリーム加古川本店<br>北近畿地区で軽自動車ならドリーム福知山店・ドリーム舞鶴店</h1>
    <p id="logo"><a href="/"><img src="/img/logo.png" alt="ドリームジャパン"></a></p>
  </div>
  <p id="btn_recruit"><a href="https://recruit.dreamjapan.jp/" class="hvr-bounce-in" target="_blank"><img src="/img/btn_recruit.png?20191126" alt="採用情報"></a></p>
  <div class="box_r">
    <p id="h_time">営業時間:AM10:00～PM8:00</p>
    <p id="sb_btn"><a href="/blog/" class="arrow hvr-bounce-in"><img src="/img/blog.png" alt="スタッフブログ"></a></p>
  </div>
</header>\n
EOD;

$nav = <<< EOD
<nav>
<ul class="clearfix inner">
	<li class="top"><a href="/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="ドリームの魅力"></a></li>
  <li class="dream"><a href="/dream/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="ドリームの魅力"></a></li>
  <li class="zaiko"><a href="https://zaiko.dreamjapan.jp/usedcar/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="最新在庫情報"></a></li>
  <li class="about"><a href="/about/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="お車の事なら"></a></li>
  <li class="shop"><a href="/shop/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="お近くの店舗"></a></li>
  <li class="album"><a href="/album/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="ドリームのお客様"></a></li>
  <li class="company"><a href="/company/" class="hvr-radial-out"><img src="/img/gnav.png?20190315" alt="会社案内"></a></li>
</ul>
</nav>\n
EOD;


$footer = <<< EOD
<footer class="fw">
<ul id="sidebtn">
  <li class="zaiko"><a href="https://zaiko.dreamjapan.jp/usedcar/"><img src="/img/sbtn_zaiko.png" alt="在庫一覧"></a></li>
  <li class="contact"><a href="/contact/"><img src="/img/sbtn_contact.png" alt="お問い合わせ"></a></li>
  <li class="shaken_con"><a href="/reserve/"><img src="/img/sbtn_shaken.png" alt="車検来店予約"></a></li>
</ul>
<p id="pagetop"><a href="#top"><img src="/img/pagetop.png" alt="このページの先頭へ"></a></p>
<div class="inner">
  <ul id="fnav" class="flex between">
    <li>
      <p><a href="/dream/">ドリームの魅力</a></p>
      <ul>
        <li><a href="/dream/#con1">ドリームとは</a></li>
        <li><a href="/dream/#con2">届出済未使用車とは</a></li>
        <li><a href="/dream/#con3">安さのヒミツ</a></li>
      </ul>
    </li><!--/ドリームの魅力-->
    <li>
      <p><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></p>
      <ul>
        <li><a href="https://zaiko.dreamjapan.jp/usedcar/">在庫車一覧</a></li>
        <!--<li><a href="/">ホームページ特典</a></li>-->
      </ul>
    </li><!--/最新在庫情報-->
    <li>
      <p><a href="/about/">お車の事なら</a></p>
      <ul>
        <li><a href="/about/syaken.php">車検</a></li>
        <li><a href="/about/bankin.php">鈑金・修理</a></li>
        <li><a href="/about/hoken.html">保険</a></li>
        <li><a href="/about/hoken.html#rental">レンタル</a></li>
      </ul>
    </li><!--/お車の事なら-->
    <li>
      <p><a href="/shop">お近くの店舗</a></p>
      <ul>
        <li><a href="/shop/#kakogawa">加古川本店</a></li>
        <li><a href="/shop/#fukuchiyama">福知山店</a></li>
        <li><a href="https://k-398.dreamjapan.jp/" target="_blank">軽アウトレット専門店</a></li>
        <li><a href="/shop/#maizuru">舞鶴店</a></li>
      </ul>
    </li><!--/お近くの店舗-->
    <li>
      <p><a href="/album/">ドリームのお客様</a></p>
      <ul>
        <li><a href="/album/">納車式</a></li>
        <!--<li><a href="/">アンケートハガキ</a></li>-->
      </ul>
    </li><!--/ドリームのお客様-->
    <li>
      <p><a href="/company/">会社案内</a></p>
      <ul>
        <li><a href="/company/#gaiyou">会社概要</a></li>
        <li><a href="/company/#enkaku">沿革</a></li>
        <li><a href="/company/#aisatu">代表あいさつ</a></li>
        <li><a href="/company/#rinen">経営理念</a></li>
        <li><a href="/company/index.html#bnrBox">系列会社一覧</a></li>
        <!--<li><a href="/">スタッフ募集</a></li>-->
      </ul>
    </li><!--/会社案内-->
  </ul>

  <ul class="snav flex">
    <!--<li><a href="/staff/" class="arrow">スタッフ一覧</a></li>-->
    <li><a href="/blog/" class="arrow">スタッフブログ</a></li>
    <li><a href="/company/privacy.html" class="arrow">プライバシーポリシー</a></li>
    <li><a href="/contact/" class="arrow">お問い合わせ</a></li>
    <li><a href="/contact/consultation.php" class="arrow">お客様相談室</a></li>
  </ul>
  <div class="f_box flex">
    <p id="f_logo"><img src="/img/logo_f.png" alt="軽未使用車専門店ドリーム"></p>
    <p id="f_time">営業時間:AM10:00～PM8:00</p>
  </div><!--/f_box-->
</div><!--/inner-->
<p id="copy" class="fw">
Copyright&copy;2016 Dream Japan.co,.ltd All Rights Reserved. Designed by <a href="https://www.tratto-brain.jp/" target="_blank">Tratto Brain</a>.</p>
</footer>
EOD;
} else { //スマホ版

// metaタグ
$meta .= <<< EOD
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta name="format-detection" content="telephone=no" />
EOD;


$css = <<< EOD
<link rel="stylesheet" href="/css/common.css" />
<link rel="stylesheet" href="/css/contents.css?202010" />
<link rel="stylesheet" href="/css/sp/base.css?20191126" />
<link rel="stylesheet" href="/css/sp/contents.css?202010" />
EOD;

// js
$js = <<< EOT
<link rel="apple-touch-icon" href="/img/sp/icon.png" />
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/script_sp.js"></script>

EOT;


// ヘッダー
$header = <<< EOD
<div id="mbox">
<ul id="open_menu">
   <li><a href="/index_sp.html">トップページ</a></li>
   <li><a href="/dream/">ドリームの魅力</a></li>
   <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
   <li><a href="/about/">お車の事なら</a></li>
   <li><a href="/shop/">お近くの店舗</a></li>
   <li><a href="/blog/">スタッフブログ</a></li>
   <li><a href="/album/">ドリームのお客様</a></li>
   <li><a href="/company/">会社案内</a></li>
   <li><a href="/contact/">お問い合わせ</a></li>
   <li><a href="/reserve/">車検来店予約</a></li>
   <li><a href="/contact/consultation.php">お客様相談室</a></li>
   <li><a href="https://recruit.dreamjapan.jp/" target="_brank">採用情報</a></li>
</ul>
</div>

<header>
  <h1><a href="/index_sp.html"><img src="/img/logo.png" width="160" alt="アドバンスドエアー"></a></h1>
<div class="header_menu"><a href="javascript:void(0);" id="menubtn" class="menu-trigger">
<span></span>
<span></span>
<span></span>
<div>menu</div>
</a>
</div>
</header>

EOD;


// フッター
$footer = <<< EOD
<footer class="clear">
<ul id="btn_nav" class="clearfix">
  <li class="zaiko"><a href="https://zaiko.dreamjapan.jp/usedcar/">在庫車一覧</a></li>
  <li class="contact"><a href="/contact/">お問い合わせ</a></li>
</ul>

  <p id="pagetop"><a href="#top" title="このページの先頭へ"><img src="/img/pagetop.png" width="60" alt="このページの先頭へ"></a></p>
  <ul class="fNav clearfix clear">
    <li><a href="/dream/">ドリームの魅力</a></li>
    <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
    <li><a href="/about/">お車の事なら</a></li>
    <li><a href="/shop/">お近くの店舗</a></li>
    <li><a href="/album/">ドリームのお客様</a></li>
    <li><a href="/company/">会社案内</a></li>
  </ul>
  <p class="sp_link_radius"><a href="/?sp=no">PCサイトはこちら</a></p>
  <div class="company clearfix inner">
    <p class="flogo"><a href="/"><img src="/img/logo.png" alt="ドリームジャパン" class="sp_img"></a></p>
    <p>Dream Japan株式会社<br>兵庫県加古川市米田町船頭字谷107-3</p>
  </div>
  <p id="copy" class="clear">Copyright&copy;2016 Dream Japan.co,.ltd All Rights Reserved.</p>
</footer>

EOD;

} //スマホ版ここまで

$footer .= <<< EOD
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72482332-32', 'auto');
  ga('send', 'pageview');

</script>
EOD;

?>
