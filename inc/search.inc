<?php

$searchjs = <<< EOD
<script type="text/javascript">
$(document).ready(function(){
  comboBoxChain('#maker', '#syamei');
});

function comboBoxChain(parent, child){
	$(parent).attr('data-chain-data','<div>' + $(child).html()).change(function(){
		$(child).children().remove().end().append($($(this).data('chainData')).find('.all, .' + $(this).val()));
	}).change();
}
</script>
EOD;

$select_shop = create_select2('shop', $_shop, $_FORM['shop'], 'up', '選択なし');
$select_maker = ''; $select_syamei = '';
foreach( $li_maker as $k => $v ){
  $ck = $k == $_FORM['maker'] ? ' selected' : '';
  $select_maker .= '<option value="'.$k.'"'.$ck.'>'.$v."</option>\n";
}
foreach( $li_syamei as $c => $a ){
  if(empty($k)){
    $select_syamei = "<option value=\"\" class=\"{$c}\">選択なし</option>\n";
  } else {
    foreach( $a as $k => $v ){
      $ck = $c == $_FORM['maker'] && $k == $_FORM['syamei'] ? ' selected' : '';
      $select_syamei .= '<option value="'.$k.'" class="'.$c.'"'.$ck.'>'.$v."</option>\n";
    }
  }
}
$select_price = create_select2('price', $li_price, $_FORM['price'], 'up', '選択なし');

$searchBox = <<< EOD
<form method="get" action="list.php">
<div id="search_box">
  <h4>車を探す</h4>
    <ul class="flex between">
<li>
<span>店舗</span>
{$select_shop}
</li>
<li>
<span>メーカー</span>
<select name="maker" id="maker">
<option value="">選択なし</option>
{$select_maker}
</select></li><!--/メーカー-->
<li>
<span>車種</span>
<select name="syamei" id="syamei">
<option value="" class="all">選択なし</option>
{$select_syamei}
</select></li><!--/車名-->

<li><span>価格帯</span>{$select_price}</li>
    </ul>
    <p class="searchBtn"><input type="submit" name="submit" value="この条件で検索"></p>
</div><!--#search_box-->
</form>
EOD;


?>