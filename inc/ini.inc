<?php
$now_y = date('n') >= 11 ? date('Y') + 1 :  date('Y');
// 年式
for($i = $now_y; $i >= $now_y-10; $i--){
    $ntdata = gengo_from_ad($i,12,"A").'('.$i.')';
    if($i == $now_y-10){ $ntdata .= "以前"; }
    $li_nensiki[$i] = $ntdata;
}

// メーカー
$li_maker[1] = 'スズキ';
$li_maker[2] = 'ダイハツ';
$li_maker[3] = 'ホンダ';
$li_maker[4] = 'ニッサン';
$li_maker[5] = '三菱';
$li_maker[6] = 'マツダ';
$li_maker[7] = 'トヨタ';
$li_maker[8] = 'スバル';
$li_maker[9] = '国産車その他';
$li_maker[10] = 'プジョー';

// 車名 スズキ
$li_syamei[1][1] = 'ワゴンR';
$li_syamei[1][2] = 'ワゴンRスティングレー';
$li_syamei[1][3] = 'アルト';
$li_syamei[1][4] = 'アルトエコ';
$li_syamei[1][5] = 'ラパン';
$li_syamei[1][6] = 'MRワゴン';
$li_syamei[1][7] = 'スペーシア';
$li_syamei[1][8] = 'ハスラー';
$li_syamei[1][16] = 'パレット';
$li_syamei[1][9] = 'エブリィワゴン';
$li_syamei[1][10] = 'ジムニー';
$li_syamei[1][11] = 'エブリィ';
$li_syamei[1][12] = 'キャリィ';
$li_syamei[1][13] = 'スイフト';
$li_syamei[1][14] = 'ソリオ';
$li_syamei[1][15] = 'イグニス';
//ダイハツ
$li_syamei[2][1] = 'ムーヴ';
$li_syamei[2][14] = 'ムーヴキャンバス';/*最新*/
$li_syamei[2][2] = 'ミラ';
$li_syamei[2][3] = 'ミライース';
$li_syamei[2][4] = 'ミラココア';
$li_syamei[2][5] = 'タント';
$li_syamei[2][6] = 'ウェイク';
$li_syamei[2][7] = 'ムーヴコンテ';
$li_syamei[2][8] = 'アトレーワゴン';
$li_syamei[2][9] = 'タントエグゼ';
$li_syamei[2][10] = 'ハイゼットトラック';
$li_syamei[2][11] = 'ハイゼットカーゴ';
$li_syamei[2][12] = 'コペン';
$li_syamei[2][13] = 'キャスト';
$li_syamei[2][15] = 'トール';
//ホンダ
$li_syamei[3][1] = 'N-ONE';
$li_syamei[3][2] = 'N-WGN';
$li_syamei[3][3] = 'N-BOX';
$li_syamei[3][7] = 'N-BOX/';
$li_syamei[3][4] = 'フィット';
$li_syamei[3][5] = 'シャトル';
$li_syamei[3][6] = 'ヴェゼル';
$li_syamei[3][8] = 'スペード';
$li_syamei[3][9] = 'ステップワゴン';
$li_syamei[3][10] = 'フリード';
$li_syamei[3][11] = 'アクティトラック';

//ニッサン
$li_syamei[4][1] = 'モコ';
$li_syamei[4][2] = 'デイズ';
$li_syamei[4][3] = 'デイズルークス';
$li_syamei[4][4] = 'NT-100クリッパー';
$li_syamei[4][5] = 'ノート';
$li_syamei[4][6] = 'マーチ';
$li_syamei[4][7] = 'エクストレイル';
//三菱
$li_syamei[5][1] = 'ekワゴン';
$li_syamei[5][2] = 'ekスペース';
$li_syamei[5][3] = 'ekカスタム';
//マツダ
$li_syamei[6][1] = 'キャロル';
$li_syamei[6][2] = 'フレア';
$li_syamei[6][3] = 'フレアワゴン';
$li_syamei[6][4] = 'フレアクロスオーバー';
//トヨタ
$li_syamei[7][1] = 'ピクシスエポック';
$li_syamei[7][2] = 'ピクシススペース';
$li_syamei[7][3] = 'ピクシストラック';
$li_syamei[7][4] = 'アクア';
$li_syamei[7][5] = 'ヴィッツ';
$li_syamei[7][6] = 'プリウス';
$li_syamei[7][7] = 'シエンタ';
$li_syamei[7][8] = 'タンク';
$li_syamei[7][9] = 'ヴォクシー';
$li_syamei[7][10] = 'ヴェルファイア';
$li_syamei[7][11] = 'C-HR';
//スバル
$li_syamei[8][1] = 'プレオ＋';
$li_syamei[8][2] = 'ステラ';
$li_syamei[8][3] = 'ルクラ';

//プジョー
$li_syamei[10][1] = '208';

//色
$li_color[] = 'ホワイト';
$li_color[] = 'パール';
$li_color[] = 'ブラック';
$li_color[] = 'グレー';
$li_color[] = 'シルバー';
$li_color[] = 'レッド';
$li_color[] = 'イエロー';
$li_color[] = 'ブルー';
$li_color[] = 'ダークブルー';
$li_color[] = 'グリーン';
$li_color[] = 'ピンク';
$li_color[] = 'ゴールド';
$li_color[] = 'オレンジ';
$li_color[] = 'ブラウン';
$li_color[] = 'パープル';
$li_color[] = 'その他';

//検査
$li_kensa = array('車検なし', '車検あり', '新車');

// 検査期限
for($i = $now_y+3; $i >= $now_y-1; $i--){
    $ktdata = gengo_from_ad($i,12,"A").'('.$i.')';
    $li_kensa_y[$i] = $ktdata;
}

//リサイクル
$li_recycle = array('リ未', 'リ済別', 'リ済込');

//シフト
$li_shift1 = array('フロア', 'コラム', 'インパネ');
$li_shift2 = array('オートマ', 'MTモード付オートマ', 'マニュアル', 'AGS');
$li_shift3 = array('CVT','3速', '4速', '5速', '6速', '7速', '8速');


//燃料
$li_fuel = array('ガソリン', 'ディーゼル', 'ハイブリッド', 'CNG', 'LPG', 'EV');

//ボディ
$li_body = array('セダン', 'スライドドア', 'ハッチバック', 'クーペ', '1BOX', 'ステーションワゴン', 'SUV', 'ワゴン', 'トラック', 'オープン');

$li_type1 = array( 1 => '軽自動車', 2 => '普通車');
$li_type2 = array( 1 => '未使用車', 2 => '中古車', 3 => '新車');


//○--
$li_umu = array( 1 => '○', 0 => '－' );

$umu = array( 1 => '有', 0 => '無' );

//エアコン
$li_air_con = array('－', 'マニュアル', 'オート');

//レザーシート
$li_l_sheat = array('－', 'ハーフレザー', 'フルレザー');

//スライドドア
$li_slide_door = array('－', '片面スライド', '両面スライド', '片面AUTOスライド', '両面AUTOスライド');

//CD/MD/ラジオ
$li_audio = array('－', 'CD', 'CD/MD', 'MD', 'ラジオ');

//カーナビ
$li_navi = array('－', 'DVD', 'HDD', 'CD', 'メモリ・SDD', 'ポータブル');

//DVDビデオ再生
$li_dvd = array('－', '可', '不可');

//TV
$li_tv = array('－', 'アナログ', 'ワンセグ', 'フルセグ');

//アルミホイール
$li_wheel = array('－', '純正', '社外');


?>