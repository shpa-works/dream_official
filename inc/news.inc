<?php
$news="&lt;p&gt;2022.2.18(金)&lt;/p&gt;

&lt;p&gt;【営業日変更のお知らせ】&lt;/p&gt;

&lt;p&gt;誠に勝手ながら下記のとおり営業日を変更させていただきます。&lt;/p&gt;

&lt;p&gt;〇2月22日（火）ドリーム全店　休業日&lt;br /&gt;
〇2月23日（水・祝）ドリーム全店　10時から営業&lt;/p&gt;

&lt;p&gt;お客様にはご迷惑をおかけいたしますが、ご理解の程宜しくお願い致します。&lt;/p&gt;

&lt;p&gt;2022.2.18(金)&lt;/p&gt;

&lt;p&gt;【最新チラシ更新】&lt;/p&gt;

&lt;p&gt;🎉ドリームMEGA熊本店グランドオープン＆決算先取セール🎉&lt;/p&gt;

&lt;p&gt;2022.2.5(土)&lt;/p&gt;

&lt;p&gt;🎉ドリームMEGA熊本店グランドオープン🎉&lt;/p&gt;

&lt;p&gt;2022.1.21(金)&lt;/p&gt;

&lt;p&gt;🎍新春初売り！最終売り尽くしセール🎍&lt;/p&gt;

&lt;p&gt;年に一度のお得な初売り期間とうとう最終セールとなりました！&lt;br /&gt;
お車検討中の方は是非この機会をお見逃しなく！&lt;br /&gt;
皆様のご来店心よりお待ちしております。&lt;/p&gt;

&lt;p&gt;2022.1.14(金)&lt;/p&gt;

&lt;p&gt;&lt;span style=&quot;background-color:rgb(255, 255, 255)&quot;&gt;🎍まだまだやります！&lt;/span&gt;新春初売りセール🎍&lt;/p&gt;

&lt;p&gt;年に一度のお得な初売り期間まだまだ継続中！&lt;br /&gt;
お車検討中の方は是非最新チラシをチェックしてください。&lt;br /&gt;
皆様のご来店心よりお待ちしております。&lt;/p&gt;

&lt;p&gt;2022.1.3(月)&lt;/p&gt;

&lt;p&gt;&lt;span style=&quot;background-color:rgb(255, 255, 255); color:rgb(51, 51, 51); font-size:13px&quot;&gt;🎍&lt;/span&gt;本日より新春初売り第2弾開催中です&lt;span style=&quot;background-color:rgb(255, 255, 255); color:rgb(51, 51, 51); font-size:13px&quot;&gt;🎍&lt;/span&gt;&lt;br /&gt;
まだまだお得な初売り期間継続中！！！&lt;/p&gt;

&lt;p&gt;ETC.バックカメラ.ナンバーフレームプレゼントやラーメン摑み取り、必ず当たるガラポン抽選会も開催中です♪&lt;/p&gt;

&lt;p&gt;皆様のご来店心よりお待ちしております!!&lt;/p&gt;

&lt;p&gt;2022.1.1(土)&lt;/p&gt;

&lt;p&gt;皆様、新年明けましておめでとうございます！&lt;br /&gt;
&lt;span style=&quot;background-color:rgb(255, 255, 255); color:rgb(51, 51, 51); font-size:13px&quot;&gt;🎍&lt;/span&gt;今年もドリームをよろしくお願い致します&lt;span style=&quot;background-color:rgb(255, 255, 255); color:rgb(51, 51, 51); font-size:13px&quot;&gt;🎍&lt;/span&gt;&lt;/p&gt;

&lt;p&gt;2022年初売りは、本日８時からのスタートです♪&lt;br /&gt;
スライド車、デザイン車など限定目玉車が勢ぞろい！！&lt;br /&gt;
更に、ETC.バックカメラ.ナンバーフレームプレゼントやラーメン掴み取り、ガラポン抽選会も開催しております！&lt;br /&gt;
大変お得なこの期間をお見逃しなく！！！&lt;/p&gt;

&lt;p&gt;2021.12.31(金）&lt;/p&gt;

&lt;p&gt;&lt;span style=&quot;color:rgb(0, 0, 0)&quot;&gt;今年も1年間、多くのご来場・ご成約いただきありがとうございました。&lt;br /&gt;
どうかみなさま、よいお年をお迎えください。&lt;/span&gt;&lt;/p&gt;

&lt;p&gt;2022年初売りは1月1日（土）朝8時からオープンいたします！&lt;br /&gt;
🎍2022年もドリームを宜しくお願いいたします🎍&lt;/p&gt;
";
?>