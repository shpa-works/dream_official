<?php
    $infoFromName="Dream Japan株式会社";
    $infoFromEmail="dream.office@kei-dream.jp";
    $infoToEmailMaizuru="dream.office@kei-dream.jp,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,dream-megakumamoto@maihon.net";
    $infoToEmailFukuchi="dream-fukuchi@maihon.net,hayataro-fukuchi@maihon.net,hayataro-fukuchi@maihon.net,hayatarofukuchi@mild.ocn.ne.jp,takayuki_kobayashi@maihon.net,dream-megakumamoto@maihon.net";
    $infoToEmailKakogawa="dream-kakogawa@spice.ocn.ne.jp,dream-kakogawa@maihon.net,takayuki_kobayashi@maihon.net,hideki.haru@maihon.net,maihon08047088501@i.softbank.jp,maihon08038364911@i.softbank.jp,dream-megakumamoto@maihon.net";
    $infoSubject="【Dream Japan株式会社】お問い合わせ受付のご確認";
    $infoMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社

ドリーム加古川本店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

ドリームMEGA熊本店
熊本県熊本市東区御領8丁目10-101
TEL:096-349-0100 / FAX:096-349-0200

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリームプレミアム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355


https://dreamjapan.jp/

**********************************************";

    $zaikoFromName="Dream Japan株式会社";
    $zaikoFromEmail="dream.office@kei-dream.jp";
    $zaikoToEmailMaizuru="dream.office@kei-dream.jp,dream-maizuru@maihon.net,takayuki_kobyashi@maihon.net,dream-megakumamoto@maihon.net";
    $zaikoToEmailFukuchi="dream-fukuchi@maihon.net,takayuki_kobayashi@maihon.net,dream-megakumamoto@maihon.net";
    $zaikoToEmailKakogawa="dream-kakogawa@spice.ocn.ne.jp,dream-kakogawa@maihon.net,takayuki_kobayashi@maihon.net,hideki.haru@maihon.net,maihon08047088501@i.softbank.jp,maihon08038364911@i.softbank.jp,dream-megakumamoto@maihon.net";
    $zaikoSubject="【Dream Japan株式会社】在庫車お問い合わせ受付のご確認";
    $zaikoMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社

ドリーム加古川本店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

ドリームMEGA熊本店
熊本県熊本市東区御領8丁目10-101
TEL:096-349-0100 / FAX:096-349-0200

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリームプレミアム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355


https://dreamjapan.jp/

**********************************************";

    $reserveFromName="Dream Japan株式会社";
    $reserveFromEmail="dream-bankin@maihon.net";
    $reserveToEmailMaizuru="dream-bankin@maihon.net,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com";
    $reserveToEmailFukuchi="hayatarofukuchi@mild.ocn.ne.jp,hayataro-fukuchi@maihon.net,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com";
    $reserveToEmailKakogawa="dream-kakogawa@spice.ocn.ne.jp,dream-kakogawa@maihon.net,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com";
    $reserveSubject="【Dream Japan株式会社】車検予約受付のご確認";
    $reserveMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社

ドリーム加古川本店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

ドリームMEGA熊本店
熊本県熊本市東区御領8丁目10-101
TEL:096-349-0100 / FAX:096-349-0200

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリームプレミアム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355


https://dreamjapan.jp/

**********************************************";

    $shakenFromName="Dream Japan株式会社";
    $shakenFromEmail="dream.office@kei-dream.jp";
    $shakenToEmailMaizuru="dream.office@kei-dream.jp,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com";
    $shakenToEmailFukuchi="dream-fukuchi@maihon.net,dream-maizuru@maihon.net,hayataro-fukuchi@maihon.net,hayatarofukuchi@mild.ocn.ne.jp,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com";
    $shakenToEmailKakogawa="dream-kakogawa@spice.ocn.ne.jp,dream-kakogawa@maihon.net,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com";
    $shakenSubject="【Dream Japan株式会社】車検来店予約受付のご確認";
    $shakenMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社

ドリーム加古川本店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

ドリームMEGA熊本店
熊本県熊本市東区御領8丁目10-101
TEL:096-349-0100 / FAX:096-349-0200

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリームプレミアム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355


https://dreamjapan.jp/

**********************************************";

    $bankinFromName="Dream Japan株式会社";
    $bankinFromEmail="carcon-maizuru@maihon.net,bpdream2017@gmail.com";
    $bankinToEmail="carcon-maizuru@maihon.net,bpdream2017@gmail.com,takayuki_kobayashi@maihon.net";
    $bankinSubject="【Dream Japan株式会社】鈑金・修理お見積予約受付のご確認";
    $bankinMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社

ドリーム加古川本店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

ドリームMEGA熊本店
熊本県熊本市東区御領8丁目10-101
TEL:096-349-0100 / FAX:096-349-0200

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリームプレミアム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355


https://dreamjapan.jp/

**********************************************";

    $consultationFromName="Dream Japan株式会社";
    $consultationFromEmail="shachoushitsu@maihon.net";
    $consultationToEmail="atsushi.kawaji@maihon.net,hideki.haru@maihon.net,shachoushitsu@maihon.net,takayuki_kobayashi@maihon.net,dream-kakogawa@maihon.net,dream-megakumamoto@maihon.net";
    $consultationSubject="【Dream Japan株式会社・お客様相談室】ご意見・ご要望のご確認";
    $consultationMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社・お客様相談室
京都府舞鶴市下福井882-20
TEL:0773-78-2222 / FAX:0773-75-4477
https://dreamjapan.jp/

**********************************************";

    $requestFromName="Dream Japan株式会社";
    $requestFromEmail="dream-fukuchi@maihon.net";
    $requestToEmail="dream-fukuchi@maihon.net,info@dreamjapan.jp,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,dream-kakogawa@maihon.net,hideki.haru@maihon.net,maihon08047088501@i.softbank.jp,maihon08038364911@i.softbank.jp,dream-megakumamoto@maihon.net";
    $requestSubject="【Dream Japan株式会社】ご希望車種お問い合わせのご確認";
    $requestSubject_c="【Dream Japan株式会社】ご希望車種お問い合わせがありました。";
    $requestMsg="お問い合わせありがとうございました。お問い合わせ頂いた内容は下記の通りです。
後日ご返答させていただきます。

%MsgData%

********************************************

Dream Japan株式会社

ドリーム加古川本店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

ドリームMEGA熊本店
熊本県熊本市東区御領8丁目10-101
TEL:096-349-0100 / FAX:096-349-0200

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリームプレミアム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355


https://dreamjapan.jp/

**********************************************";

    $couponFromName="Dream Japan株式会社";
    $couponFromEmail="dream.office@kei-dream.jp";
    $couponToEmail="dream.office@kei-dream.jp,dream-maizuru@maihon.net,takayuki_kobayashi@maihon.net,k.nakamura.dream@gmail.com,dream-fukuchi@maihon.net,dream-kakogawa@spice.ocn.ne.jp,dream-kakogawa@maihon.net,hideki.haru@maihon.net,maihon08047088501@i.softbank.jp,maihon08038364911@i.softbank.jp,dream-kumamoto@maihon.net";
    $couponSubject="【Dream Japan株式会社】WEBクーポンのお問い合わせありがとうございます。";
    $couponMsg="お問い合わせありがとうございました。
ご来店・ご商談時、スタッフにクーポンのご呈示をお願いいたします。
下記のURLよりクーポンを表示、または画像を保存してください。

https://dreamjapan.jp/coupon/coupon.jpg

ご入力頂いた内容は下記の通りです。

%MsgData%

********************************************

Dream Japan株式会社
ドリーム舞鶴店
京都府舞鶴市下福井51
TEL:0773-78-3399 / FAX:0773-78-3355

ドリーム福知山店
京都府福知山市篠尾961-44
TEL:0773-23-0031 / FAX:0773-23-0072

ドリーム加古川店
兵庫県加古川市米田町船頭字谷107-3
TEL:079-434-3000 / FAX:079-432-0002

https://dreamjapan.jp/

**********************************************";
?>