<?php
/*
Template Name: サービス - 鈑金・修理
*/
?>


<?php
$setPath= "";
$pageTitle = "鈑金・修理｜サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="service repair">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">鈑金・修理</span><br>
        <span class="en">SHEET METAL / REPAIR</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/service/"><span itemprop="name">サービス</span></a>
        <meta itemprop="position" content="2">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">鈑金・修理</span>
        <meta itemprop="position" content="3">
      </li>
    </ol>

    <section class="sec01 perks">
      <div class="wrapper">
        <div class="perks-inner">
          <h3 class="perks-title title_obi">うれしい3大特典</h3>
          <ol class="perks-list">
            <li class="perks-list-item">
              <dl class="perks-list-inner">
                <dt class="perks-list-numb"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/perks_num01.png" alt="特典１"></dt>
                <dt class="perks-list-title">インターネット割引</dt>
                <dd class="perks-list-text">ホームページよりお申し込みの方は<br>
                  鈑金工賃を10％割引いたします。</dd>
              </dl>
            </li>
            <li class="perks-list-item">
              <dl class="perks-list-inner">
                <dt class="perks-list-numb"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/perks_num02.png" alt="特典２"></dt>
                <dt class="perks-list-title">見積即決割引</dt>
                <dd class="perks-list-text">ご来店見積もり時に即決で鈑金修理のご予約をいただいたお客様は<br>
                  鈑金工賃を1000円引きいたします。</dd>
              </dl>
            </li>
            <li class="perks-list-item">
              <dl class="perks-list-inner">
                <dt class="perks-list-numb"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/perks_num03.png" alt="特典３"></dt>
                <dt class="perks-list-title">代車不要割引</dt>
                <dd class="perks-list-text">鈑金修理時に代車が不要のお客様は、<br>鈑金工賃を3000円引きいたします。
                  <!-- <br>
                  <span class="note">※部品等は、メーカーの保証範囲に準ずる<br>
                    ※万が一、修理箇所の塗装はがれ等があった場合は、無料で再補修させていただきます。</span> -->
                </dd>
              </dl>
            </li>
          </ol>
        </div><!-- /.perks-inner -->
      </div><!-- /.wrapper -->
    </section><!-- /.preks -->

    <section class="sec02 point">
      <div class="wrapper">
        <h3 class="point-title title_obi">小キズから、大キズまで<span>確かな技術で修復！</span></h3>
        <ol class="point-list">
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/point_pic01.jpg" alt="Point1"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">作業時間を大幅短縮</dt>
              <dd class="point-list-text">
                <p>当店はカーコンビニ倶楽部が独自に開発した技術や専用のツールで、実作業時間やお客様の待ち時間を約１／４の作業時間に大幅短縮しています。</p>
              </dd>
            </dl>
          </li>
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/point_pic02.jpg" alt="Point2"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">安心事前見積</dt>
              <dd class="point-list-text">
                <p>全国チェーンのカーコンビニ倶楽部独自の、お見積りコンピューターシステムの事前見積りをお出しするので、お客様が納得されてからの作業となります。</p>
              </dd>
            </dl>
          </li>
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/point_pic03.jpg" alt="Point3"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">ハイクオリティな修理を低価格で！</dt>
              <dd class="point-list-text">
                <p>独自技術や専用のツールで、作業時間の短縮化、リサイクルパーツの利用等でのコスト削減をお客様への料金に還元しています。</p>
              </dd>
            </dl>
          </li>
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/point_pic04.jpg" alt="Point4"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">仕上がりに自信あり</dt>
              <dd class="point-list-text">
                <p>どんな色でも分析してつくるから、色・仕上がりが超キレイです。当社は更なる技術向上を追及しており、どんな色でも対応でき、キレイに仕上げることができます。</p>
              </dd>
            </dl>
          </li>
        </ol>
        <div class="color_box_content clearfix">
          <div class="box height">
            <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/discount_num01.png" alt="割引01"></div>
            <div class="tit"><p>インターネット割引</p></div>
            <p class="height02">ホームページよりお見積りをお申込みいただくと鈑金工賃を10％割引いたします。</p>
            <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/discount_price01.png" alt="修理工賃より10%OFF"></div>
          </div>
          <div class="box height">
            <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/discount_num02.png" alt="割引02"></div>
            <div class="tit"><p>見積即決割引</p></div>
            <p class="height02">ご来店見積もり時に即決で鈑金修理のご予約をいただいたお客様は鈑金工賃を1000円引きいたします。</p>
            <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/discount_price02.png" alt="修理工賃より1,000円OFF"></div>
          </div>
          <div class="box height">
            <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/discount_num03.png" alt="割引03"></div>
            <div class="tit"><p>代車不要割引</p></div>
            <p class="height02">鈑金修理時に代車が不要のお客様は、鈑金工賃を3000円引きいたします。</p>
            <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/discount_price03.png" alt="修理工賃より3,000円OFF"></div>
          </div>
        </div>
      </div><!-- /.wrapper -->
    </section><!-- /.point -->

    <section class="sec03">
      <div class="wrapper">
        <div class="handling">
          <h3 class="handling-title">取扱店舗</h3>
          <ul class="handling-list">
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/shop_kakogawa.png" alt="カーコンビニ倶楽部加古川店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_carconveni.png" alt="カーコンビニ倶楽部加古川店"><span>加古川店</span></p>
              <p class="add">〒675-0053<br>
                兵庫県加古川市米田町船頭字谷107-3</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>079-434-3000</dd>
                <dt>営業時間</dt>
                <dd>10時～20時</dd>
                <dt>定休日</dt>
                <dd>水曜日</dd>
              </dl>
              <p class="link"><a href="https://bankin.dreamjapan.jp/" target="_blank">詳しくはこちら</a></p>
            </li>
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/repair/shop_maizuru.png" alt="カーコンビニ倶楽部舞鶴店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_carconveni.png" alt="カーコンビニ倶楽部舞鶴店"><span>舞鶴店</span></p>
              <p class="add">〒624-0946<br>
                京都府舞鶴市下福井51</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0773-77-5533</dd>
                <dt>営業時間</dt>
                <dd>10時～20時</dd>
                <dt>定休日</dt>
                <dd>水曜日</dd>
              </dl>
              <p class="link"><a href="https://bankin.dreamjapan.jp/kakogawa/" target="_blank">詳しくはこちら</a></p>
            </li>
          </ul>
        </div><!-- /.handling -->
      </div><!-- /.wrapper -->
    </section><!-- /.sec04 -->

    <section>
      <?php require_once($setPath.'lib/include/service.php'); ?>
    </section>

    <?php require_once($setPath.'lib/include/footer.php'); ?>

  </body>
</html>
