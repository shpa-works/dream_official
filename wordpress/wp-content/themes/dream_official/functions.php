<?php

/* wp管理画面のメニューを利用 */
add_theme_support( 'menus' );

/* モバイル端末分岐 */
function is_mobile() {
    $useragents = array(
        'iPhone',          // iPhone
        'iPod',            // iPod touch
        '^(?=.*Android)(?=.*Mobile)', // 1.5+ Android
        'dream',           // Pre 1.5 Android
        'CUPCAKE',         // 1.5+ Android
        'blackberry9500',  // Storm
        'blackberry9530',  // Storm
        'blackberry9520',  // Storm v2
        'blackberry9550',  // Storm v2
        'blackberry9800',  // Torch
        'webOS',           // Palm Pre Experimental
        'incognito',       // Other iPhone browser
        'webmate'          // Other iPhone browser
    );
    $pattern = '/'.implode('|', $useragents).'/i';
    return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

//サイドバーにウィジェット追加
function widgetsidebar_init() {
  register_sidebar(array(
    'name'=>'サイドバー',
    'id' => 'sidebar',
    'before_widget'=>'
      <div id="%1$s" class="sidebar-inner">',
    'after_widget'=>'</div><!-- /.sidebar-inner -->',
    'before_title' => '<h3 class="sidebar-title">',
    'after_title' => '</h3>'
  ));
}
add_action( 'widgets_init', 'widgetsidebar_init' );

// ページネーションのカスタマイズ
function custom_pagination_html( $template ) {
    $template = '%3$s';
    return $template;
}
add_filter('navigation_markup_template','custom_pagination_html');


//conatct form sevenのメールアドレス確認設定
add_filter( 'wpcf7_validate_email', 'wpcf7_validate_email_filter_confrim', 11, 2 );
add_filter( 'wpcf7_validate_email*', 'wpcf7_validate_email_filter_confrim', 11, 2 );
function wpcf7_validate_email_filter_confrim( $result, $tag ) {
    $type = $tag['type'];
    $name = $tag['name'];
    if ( 'email' == $type || 'email*' == $type ) {
        if (preg_match('/(.*)_confirm$/', $name, $matches)){ //確認用メルアド入力フォーム名を ○○○_confirm としています。
            $target_name = $matches[1];
                $posted_value = trim( (string) $_POST[$name] ); //前後空白の削除
                $posted_target_value = trim( (string) $_POST[$target_name] ); //前後空白の削除
            if ($posted_value != $posted_target_value) {
                $result->invalidate( $tag,"確認用のメールアドレスが一致していません");
            }
        }
    }
    return $result;
}
