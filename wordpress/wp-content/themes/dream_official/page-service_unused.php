<?php
/*
Template Name: サービス - 未使用車販売
*/
?>


<?php
$setPath= "";
$pageTitle = "未使用車販売｜サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="service unused">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <div id="keyVisual">
    <h2 class="page_title">
      <span class="jp">未使用車販売</span><br>
      <span class="en">UNUSED CAR SALES</span>
    </h2>
  </div>

  <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
      <meta itemprop="position" content="1">
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?php echo home_url(); ?>/service/"><span itemprop="name">サービス</span></a>
      <meta itemprop="position" content="2">
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <span itemprop="name">未使用車販売</span>
      <meta itemprop="position" content="3">
    </li>
  </ol>

  <section class="sec01">
    <div class="wrapper">
      <div class="top_txt">
        <div class="img">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_title.png" alt="ドリームの登録済未使用車とは">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sp/sec01_title.png" alt="ドリームの登録済未使用車とは">
        </div>
        <p class="txt">ドリームが扱っている未使用車の7つの条件です。<br>これを満たしていない車は未使用車として販売しておりません。</p>
      </div>

      <div class="color_board01 green">
        <h4 class=mintit>未使用車の７つの条件</h3>
        <ul>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number01.png" alt="条件01"></div>
            <div class="txtbox">
              <p class="tit">走行距離はは原則10㎞以内</p>
              <p class="txt">走行距離は原則10㎞以内です。(社内・店舗間移動の為、10㎞少し過ぎる事もあります。)<br>他店では100㎞走行している車でさえ未使用車と呼ぶ事もあるようです。</p>
            </div>
          </li>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number02.png" alt="条件02"></div>
            <div class="txtbox">
              <p class="tit">毎月未使用車を大量仕入れ</p>
              <p class="txt">ドリームは毎月500台程度、未使用車を仕入れています。1度に大量に仕入れることで、お客様にお得な未使用車を提供させていただいています。</p>
            </div>
          </li>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number03.png" alt="条件03"></div>
            <div class="txtbox">
              <p class="tit">メーカー代理店・協力店</p>
              <p class="txt">ホンダ・スズキ・ダイハツ・トヨタ・日産・三菱・マツダと日本全メーカーから仕入れる為、各メーカーとも代理店・協力店契約をしており、メーカーの基準に沿った保証、修理等の対応が出来るお車です。</p>
            </div>
          </li>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number04.png" alt="条件04"></div>
            <div class="txtbox">
              <p class="tit">新車と同保証・同サービス</p>
              <p class="txt">ドリームの未使用車は全車新車購入と同じ保証、サービスが適応されます。</p>
            </div>
          </li>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number05.png" alt="条件05"></div>
            <div class="txtbox">
              <p class="tit">下取りも新車と同査定基準</p>
              <p class="txt">下取り時にも新車同様の査定基準で行われる為お客様が新車と比較し、何か不利になることは一切ございません。</p>
            </div>
          </li>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number06.png" alt="条件06"></div>
            <div class="txtbox">
              <p class="tit">最短3日納車</p>
              <p class="txt">最短3日で納車できます。</p>
            </div>
          </li>
          <li>
            <div class="number"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec01_number07.png" alt="条件07"></div>
            <div class="txtbox">
              <p class="tit">保険も不利益一切なし</p>
              <p class="txt">任意保険でも新車特約は使え、自動車保険においても一切不利益な事はありません。</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>


  <section class="sec02">
    <div class="wrapper">
      <h3 class="title_obi">未使用車の<span>３つのメリット</span></h3>
      <div class="color_box_content clearfix">
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec02_mintit01.png" alt="メリット01"></div>
          <div class="tit"><p>値段は安いのに新品同様</p></div>
          <p class="height02">未使用車は新車から名義登録だけをした状態の車で、ほぼ新車と同様の車です。</p>
          <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec02_parts01.png" alt=""></div>
        </div>
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec02_mintit02.png" alt="メリット02"></div>
          <div class="tit"><p>新車同等の保証で安心</p></div>
          <p class="height02">新車同等の保証が適用されますので(有料)アフターサービスも安心です。</p>
          <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec02_parts02.png" alt=""></div>
        </div>
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec02_mintit03.png" alt="メリット03"></div>
          <div class="tit"><p>すぐ乗れるスピード納車</p></div>
          <p class="height02">新車の場合納車まで1～２か月ですが、届出済未使用車なら最短当日納車も可能！</p>
          <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec02_parts03.png" alt=""></div>
        </div>
      </div>
    </div>
  </section>


  <section class="sec03">
    <div class="wrapper">
      <h3 class="title_obi">未使用車は<span>本当にお得なの？</span></h3>
      <div class="car_img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec03_parts01.png" alt=""></div>
      <div class="list_content">
        <p class="title"><span>人気No.1のN-BOXで比較した場合</span></p>
        <div class=table_holder>
          <table>
            <tr>
              <th class="type01">購入後5年間の<br>トータルコスト</th>
              <th class="type02">ドリームの未使用車</th>
              <!-- <th class="type02">新車</th> -->
              <th class="type02">5年落ち車</th>
            </tr>
            <tr class="even">
              <th class="type03">車両価格</th>
              <td>119.8万円</td>
              <!-- <td>144.4万円</td> -->
              <td>95万円</td>
            <tr>
            <tr>
              <th class="type03">諸費用</th>
              <td>20万円<br><span>※プレミアムパック</span></td>
              <!-- <td>13万円</td> -->
              <td>18万円</td>
            <tr>
            <tr class="even">
              <th class="type03">車検</th>
              <td>ドリームなら<br>基本料金+代行手数料<br>2回分０円</td>
              <!-- <td>8万円</td> -->
              <td>8～12万円</td>
            <tr>
            <tr>
              <th class="type03">付属品</th>
              <td>マット＆バイザー＆<br>コーディング0円<br>その他同じ</td>
              <!-- <td>マット＆バイザー約4万円<br>コーディング約5万円<br>その他同じ</td> -->
              <td>マット＆バイザー約4万円<br>コーディング約5万円<br>その他同じ</td>
            <tr>
            <tr class="even">
              <th class="type03">保証</th>
              <td>０円</td>
              <!-- <td>０円</td> -->
              <td>有料 約3万円</td>
            <tr>
            <tr>
              <th class="type03">任意保険</th>
              <td>安い<br><span>※新車特約/安全装置</span></td>
              <!-- <td>安い</td> -->
              <td>高い</td>
            <tr>
            <tr class="even">
              <th class="type03">ガソリン代</th>
              <td>安い<br><span>※燃費が良い</span></td>
              <!-- <td>安い</td> -->
              <td>高い</td>
            <tr>
            <tr>
              <th class="type03">かっこ良さ・満足感</th>
              <td>最高(即納車)</td>
              <!-- <td>最高(２か月)</td> -->
              <td>普通(即納出来ない10日)</td>
            <tr>
            <tr class="even">
              <th class="type03">5年後下取り</th>
              <td>80万円</td>
              <!-- <td>80万円</td> -->
              <td>10年落ちの為<br>約5～10万円</td>
            <tr>
            <tr>
              <th class="type03">その他オプション</th>
              <td>15万円</td>
              <!-- <td>15万円</td> -->
              <td>15万円</td>
            <tr>
            <tr class="last">
              <th class="type03">お得額合計</th>
              <td class="img01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec03_table_txt01.png" alt=""></td>
              <!-- <td><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec03_table_txt02.png" alt=""></td> -->
              <td><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/unused/sec03_table_txt03.png" alt=""></td>
            <tr>
          </table>
        </div>
      </div>
    </div>
  </section>


  <section class="stock_bnr">
    <div class="wrapper">
      <a href="https://zaiko.dreamjapan.jp/usedcar/">
        <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_stock_check.png" alt="最新在庫情報をチェック！">
        <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_stock_check.png" alt="最新在庫情報をチェック！">
      </a>
    </div>
  </section>


  <section>
    <?php require_once($setPath.'lib/include/service.php'); ?>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
</body>
</html>
