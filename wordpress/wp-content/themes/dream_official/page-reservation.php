<?php
/*
Template Name: 車検ご予約
*/
?>

<?php
$setPath= "";
$pageTitle = "車検ご予約";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.css" integrity="sha512-GgUcFJ5lgRdt/8m5A0d0qEnsoi8cDoF0d6q+RirBPtL423Qsj5cI9OxQ5hWvPi5jjvTLM/YhaaFuIeWCLi6lyQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>

  <body class="contact">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">車検ご予約</span><br>
        <span class="en">VEHICLE INSPECTION RESERVATION</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">車検ご予約</span>
        <meta itemprop="position" content="2">
      </li>
    </ol>
    <?php if ( is_page('reservation') ) : ?>
      <section class="sec01 tel">
        <div class="wrapper">
          <h2 class="sec01-title title_obi">お電話でご予約</h2>
          <div class="holder clearfix">
            <div class="box height">
              <p class="title"><span>車検の速太郎 加古川店</span></p>
              <div class="phone">
                <a href="tel:0037-6000-3399">0037-6000-3399</a>
              </div><!--/.phone -->
            </div><!-- /. box --->

            <div class="box height">
              <p class="title"><span>車検の速太郎 福知山店</span></p>
              <div class="phone">
                <a href="tel:0037-6000-3176">0037-6000-3176</a>
              </div><!--/.phone -->
            </div><!-- /. box --->

            <div class="box height">
              <p class="title"><span>ドリーム車検 舞鶴店</span></p>
              <div class="phone">
                <a href="tel:0120-762-202">0120-762-202</a>
              </div><!--/.phone -->
            </div><!-- /. box --->
          </div>
        </div><!-- /.wrapper -->
      </section><!-- /.sec01 -->
    <?php endif; ?>

    <section class="sec02">
      <div class="wrapper">
        <?php
        if (is_page('reservation')) {
            echo '<h2 class="sec02-title title_obi">お問い合わせフォーム</h2>';
          } else if (is_page('reservation--confirm')) {
            echo '<h2 class="sec02-title title_obi">入力内容確認</h2>';
          } else if (is_page('reservation-thanks')) {
            echo '<h2 class="sec02-title title_obi">送信完了</h2>';
          };
         ?>

        <div class="form">
          <?php the_content(); ?>
          <?php if ( is_page('reservation-thanks') ) : ?>
            <p class="thanks-text">
              お問い合わせいただきありがとうございました。<br>
              内容は送信されました。
            </p>
          <?php endif; ?>
        </div>

        <?php /* ?>
        <form class="form" action="" method="post" enctype="">
			    <table class="form-table">
  					<tr>
              <th>お名前<span class="required">【必須】</span></th>
              <td>
                <input type="text" name="お名前">
              </td>
  					</tr>
            <tr>
              <th>フリガナ<span class="required">【必須】</span></th>
              <td>
                <input type="text" name="フリガナ">
              </td>
  					</tr>
            <tr>
              <th>電話番号<span class="required">【必須】</span></th>
              <td>
                <input type="tel" name="電話番号">
              </td>
  					</tr>
            <tr>
              <th>メールアドレス<span class="required">【必須】</span></th>
              <td>
                <input type="email" name="メールアドレス">
                <span class="note">※携帯メールアドレスを登録する際に返信をご希望の場合は、パソコンからのメールが受信できるよう<br>
                  「メールドメイン指定受信」を解除のうえ送信ください。</span>
              </td>
  					</tr>
            <tr>
              <th>メールアドレス確認<span class="required">【必須】</span></th>
              <td>
                <input type="email" name="メールアドレス確認">
              </td>
  					</tr>
            <tr>
              <th>お問い合わせ店舗</th>
              <td>
                <input type="radio" id="" name="お問い合わせ店舗" checked><span>加古川本店</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>福知山店</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>舞鶴店</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>軽アウトレット店</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>熊本店</span>
              </td>
            </tr>
            <tr>
              <th>ご来店希望日</th>
              <td>
                <input type="date" value="" name="ご来店希望日">
                <input type="time" name="">
              </td>
            </tr>
            <tr>
              <th>ご用件<span class="required">【必須】</span></th>
              <td>
                <input type="radio" id="" name="お問い合わせ店舗" checked><span>ご来店予約</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>在庫確認</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>ローン確認</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>オンライン商談</span>
                <input type="radio" id="" name="お問い合わせ店舗"><span>その他</span>
              </td>
            </tr>
            <tr class="text_area">
              <th>お問い合わせ内容<span class="required">【必須】</span></th>
              <td>
                <textarea name="お問い合わせ内容" rows="5"></textarea>
              </td>
            </tr>
			    </table>
          <div class="consent">
            <p class="consent-text">
              「<a href="">プライバシーポリシー</a>」をご一読の上、内容に同意いただける場合は「同意する」にチェックし、内容確認画面にお進みください。
            </p>
            <p class="consent-button"><input type="checkbox" name=""> 同意する</p>

          </div><!-- /.consent -->
          <p class="submit-button"><input type="image" value="送信" class="submit" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/contact/button_confirm.png" /></p>
  			</form>
        <?php */ ?>
      </div>
    </section><!-- /.sec02 -->

    <?php require_once($setPath.'lib/include/footer.php'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.js" integrity="sha512-WHnaxy6FscGMvbIB5EgmjW71v5BCQyz5kQTcZ5iMxann3HczLlBHH5PQk7030XmmK5siar66qzY+EJxKHZTPEQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
      //Library jquery-timepicker
      $('.timepicker').timepicker(
        {
          'timeFormat': 'H:i',
          'minTime': '10:00',
          'maxTime': '20:00',
          'step': 10,
        }
      );
      // 初期表示時の文字色変更
      $(window).on('load', function () {
        $.each($('input[type="date"],input[type="time"]'), (index, datebox) => {
          datebox.style.color = (datebox.value) ? 'black' : 'white';
        });
      });
      $(function () {
        // フォーカス取得時は入力用にいったん色を付ける
        $('input[type=date],input[type=time]').focus(function (event) {
          this.style.color = 'black';
        });
      });
    </script>
  </body>
</html>
