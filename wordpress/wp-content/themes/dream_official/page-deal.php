<?php
/*
Template Name: 今週のお得情報
*/
?>

<?php
$setPath= "";
$pageTitle = "今週のお得情報";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.css" integrity="sha512-LVOzFPLcBUppn3NOx8FXJkh2TawWu/jk9ynbntKID6cjynQsfqmHlUbH72mjAwZXsu0LOLw26JoiC0qHJde70Q==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" integrity="sha512-jRxwiuoe3nt8lMSnOzNEuQ7ckDrLl31dwVYFWS6jklXQ6Nzl7b05rrWF9gjSxgOow5nFerdoN6CBB4gY5m5nDw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="deal">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <div id="keyVisual">
    <h2 class="page_title">
      <span class="jp">今週のチラシ</span><br>
      <span class="en">THIS WEEK’S DEALS</span>
    </h2>
  </div>

  <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
      <meta itemprop="position" content="1">
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <span itemprop="name">今週のお得情報</span>
      <meta itemprop="position" content="2">
    </li>
  </ol>

  <section class="sec01">
    <div class="wrapper">
      <div class="board">
        <div class="car01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/sec01_parts01.png" alt=""></div>
        <div class="car02"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/sec01_parts02.png" alt=""></div>
        <div class="car03"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/sec01_parts03.png" alt=""></div>

        <div class="box" id="chirashi_kakogawa">
          <div class="tit_area">
            <div class="title_wrap01">
              <h3 class="jp_tit">本店（加古川店）</h3>
              <div class="en_tit"><p>KAKOGAWA</p></div>
            </div>
          </div>
          <div class="chirashi_area clearfix">
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kakogawa_omote.jpg" alt=""></div>
              <div class="btn"><a href="#modal01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal01">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kakogawa_omote.jpg" alt=""></p>
              </div>
            </div>
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kakogawa_ura.jpg" alt=""></div>
              <div class="btn"><a href="#modal02"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal02">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kakogawa_ura.jpg" alt=""></p>
              </div><!-- /.remodal -->
            </div>
          </div>
        </div><!--- 本店（加古川店） --->

        <div class="box" id="chirashi_fukuchiyama">
          <div class="tit_area">
            <div class="title_wrap01">
              <h3 class="jp_tit">福知山店</h3>
              <div class="en_tit"><p>FUKUCHIYAMA</p></div>
            </div>
          </div>
          <div class="chirashi_area clearfix">
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_fukuchiyama_omote.jpg" alt=""></div>
              <div class="btn"><a href="#modal03"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal03">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_fukuchiyama_omote.jpg" alt=""></p>
              </div><!-- /.remodal -->
            </div>
            <!-- <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_fukuchiyama_ura.jpg" alt=""></div>
              <div class="btn"><a href="#modal04"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal04">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_fukuchiyama_ura.jpg" alt=""></p>
              </div>
            </div> -->
          </div>
        </div><!--- 福知山店 --->

        <div class="box" id="chirashi_premium">
          <div class="tit_area">
            <div class="title_wrap01">
              <h3 class="jp_tit">ドリーム プレミアム</h3>
              <div class="en_tit"><p>DREAM PREMIUM</p></div>
            </div>
          </div>
          <div class="chirashi_area clearfix">
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_premium_omote.jpg" alt=""></div>
              <div class="btn"><a href="#modal05"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal05">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_premium_omote.jpg" alt=""></p>
              </div><!-- /.remodal -->
            </div>
            <!-- <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_premium_ura.jpg" alt=""></div>
              <div class="btn"><a href="#modal06"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal06">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_premium_ura.jpg" alt=""></p>
              </div>
            </div> -->
          </div>
        </div><!--- ドリーム プレミアム --->

        <div class="box" id="chirashi_outlet">
          <div class="tit_area">
            <div class="title_wrap01">
              <h3 class="jp_tit">軽アウトレット専門店</h3>
              <div class="en_tit"><p>OUTLET SPECIALTY STORE</p></div>
            </div>
          </div>
          <div class="chirashi_area clearfix">
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_outlet_omote.jpg" alt=""></div>
              <div class="btn"><a href="#modal07"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal07">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_outlet_omote.jpg" alt=""></p>
              </div>
            </div>
            <!-- <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_outlet_ura.jpg" alt=""></div>
              <div class="btn"><a href="#modal08"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal08">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_outlet_ura.jpg" alt=""></p>
              </div>
            </div> -->
          </div>
        </div>
        <!--- 軽アウトレット専門店 --->

        <div class="box" id="chirashi_kumamoto">
          <div class="tit_area">
            <div class="title_wrap01">
              <h3 class="jp_tit">MEGA熊本店</h3>
              <div class="en_tit"><p>MEGA KUMAMOTO</p></div>
            </div>
          </div>
          <div class="chirashi_area clearfix">
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kumamoto_omote.jpg" alt=""></div>
              <div class="btn"><a href="#modal09"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal09">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kumamoto_omote.jpg" alt=""></p>
              </div><!-- /.remodal -->
            </div>
            <div class="chirashi_box">
              <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kumamoto_ura.jpg" alt=""></div>
              <div class="btn"><a href="#modal10"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/click_btn.png" alt=""></a></div>
              <div class="remodal" data-remodal-id="modal10">
                <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kumamoto_ura.jpg" alt=""></p>
              </div><!-- /.remodal -->
            </div>
          </div>
        </div><!--- 熊本店 --->
      </div>

    </div>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js" integrity="sha512-a/KwXZUMuN0N2aqT/nuvYp6mg1zKg8OfvovbIlh4ByLw+BJ4sDrJwQM/iSOd567gx+yS0pQixA4EnxBlHgrL6A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>
</html>
