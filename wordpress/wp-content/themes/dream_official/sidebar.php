<div class="sidebar">

  <section class="container newpost">
    <h4 class="sidebar-title"><span>最近の投稿</span></h4>
    <div class="sidebar-inner">
      <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 6,
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) :
      ?>
        <ul class="newpost-list">
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <li class="newpost-list-item"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        </ul>
      <?php endif; ?>
    </div><!-- /.sidebar-inner -->
  </section><!-- /.container -->

  <section class="container archive">
    <h4 class="sidebar-title"><span>アーカイブ</span></h4>
    <div class="sidebar-inner archive-inner">
      <p class="archive-box">
        <select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'>
          <option value=""><?php echo attribute_escape(__('月を選択してください')); ?></option>
          <?php wp_get_archives (array(
          'type' => 'monthly',
          'format' => 'option',
          'show_post_count' => '1'
          ));
          ?>
        </select>
      </p>
    </div><!-- /.sidebar-inner -->
  </section><!-- /.container -->

  <section class="container category">
    <h4 class="sidebar-title"><span>カテゴリー</span></h4>
    <div class="sidebar-inner">
      <ul class="category-list">
        <?php
          $categories = get_categories();
          foreach ($categories as $category) {
            echo '<li class="category-list-item"><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
          }
          ?>
      </ul>
    </div><!-- /.sidebar-inner -->
  </section><!-- /container -->

</div><!-- /. sidebar -->
