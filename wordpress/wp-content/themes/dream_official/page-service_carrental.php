<?php
/*
Template Name: サービス - レンタカー
*/
?>

<?php
$setPath= "";
$pageTitle = "レンタカー｜サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="service carrental">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">レンタカー</span><br>
        <span class="en">CAR RENTAL</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/service/"><span itemprop="name">サービス</span></a>
        <meta itemprop="position" content="2">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">レンタカー</span>
        <meta itemprop="position" content="3">
      </li>
    </ol>

    <section class="sec01">
      <div class="wrapper">
        <h2 class="rental-logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/carrental/sec01_title01.png" alt="Jネットレンタカー"></h2>
        <div class="handling jnet">
          <h3 class="handling-title">取扱店舗</h3>
          <ul class="handling-list">
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_maizuru03.jpg" alt="Jネットレンタカー舞鶴店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_jnet.png" alt="Jネットレンタカー"><span>舞鶴店</span></p>
              <p class="add">〒624-0821<br>
                京都府舞鶴市公文名12</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0773-78-2266</dd>
                <dt>営業時間</dt>
                <dd>9時～20時</dd>
                <dt>定休日</dt>
                <dd>無し（年末年始休業）</dd>
              </dl>
              <p class="note">＼JR西舞鶴駅、舞鶴西港より無料送迎サービス中／</p>
              <p class="link"><a href="https://www.j-netrentacar.co.jp/kyoto/maizuru/" target="_blank">レンタカーのご予約はこちら</a></p>
            </li>
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_fukuchiyama01.jpg" alt="Jネットレンタカー福知山店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_jnet.png" alt="Jネットレンタカー福知山店"><span>福知山店</span></p>
              <p class="add">〒620-0000<br>
                京都府福知山市字堀2486-1</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0773-25-3301</dd>
                <dt>営業時間</dt>
                <dd>9時～18時30分</dd>
                <dt>定休日</dt>
                <dd>無し（年末年始休業）</dd>
              </dl>
              <p class="note">＼JR福知山駅より無料送迎サービス中／</p>
              <p class="link"><a href="https://www.j-netrentacar.co.jp/kyoto/fukuchiyama/" target="_blank">レンタカーのご予約はこちら</a></p>
            </li>
          </ul>
        </div><!-- /.handling -->
        <h2 class="rental-logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/carrental/sec01_title02.png" alt="京都ビジネスレンタカー舞鶴店"></h2>
        <div class="handling kbrentacar">
          <h3 class="handling-title">取扱店舗</h3>
          <ul class="handling-list">
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_maizuru03.jpg" alt="京都ビジネスレンタカー舞鶴店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_kbrentacar.png" alt="京都ビジネスレンタカー舞鶴店"><span>舞鶴店</span></p>
              <p class="add">〒624-0821<br>
                京都府舞鶴市公文名12</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0120-78-2260（通話料無料）</dd>
                <dt>営業時間</dt>
                <dd>9時～20時</dd>
                <dt>定休日</dt>
                <dd>無し（年末年始休業）</dd>
              </dl>
              <p class="link"><a href="https://kyoto-brc.com/" target="_blank">法人向けレンタカーご予約はこちら</a></p>
            </li>
          </ul>
        </div><!-- /.handling -->
      </div><!-- /.wrapper -->
    </section><!-- /.sec01 -->

    <section>
      <?php require_once($setPath.'lib/include/service.php'); ?>
    </section>

    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
