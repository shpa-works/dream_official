<?php
/*
Template Name: 会社案内
*/
?>

<?php
$setPath= "";
$pageTitle = "会社案内";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="about">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">会社案内</span><br>
        <span class="en">ABOUT</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="item"><span itemprop="name">会社案内</span>
        <meta itemprop="position" content="2">
      </li>
    </ol>

    <section class="sec01 greeting">
      <div class="wrapper">
        <h2 class="greeting-title title_obi">代表あいさつ</h2>
        <div class="greeting-inner">
          <div class="text-wrap">
            <p class="text">DreamJapan株式会社のＨＰにお立ち寄りいただき、誠にありがとうございます。<br>
              弊社は、日本一の軽自動車販売会社になるという目標のもと、<br>
              全社員で真面目に日々努力を積み重ねている会社です。</p>
            <p class="text">お客様の御期待を裏切ることはございません。<br>
            グループ会社の経験・知識を最大限活用し、日本一の軽自動車の販売会社にしてまいります。</p>
            <p class="text">今後もより一層努力し、必ずドリームに出会えて良かったと思える会社にして参ります。<br>
            軽自動車の購入をお考えの方、一度、弊社店舗・店員にご相談よろしくお願いいたします。</p>
          </div><!-- /.text-wrap -->
          <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/about/greeting_pic.png" alt="代表取締役　三方　圭一">
            <figcaption>代表取締役　三方　圭一</figcaption>
          </figure>
        </div><!-- /.greeting-inner -->
        <div class="container">
          <div class="wrap">
            <dl class="container-inner">
              <dt class="container-title">経営理念</dt>
              <dd class="container-text">努力は己を裏切らない。</dd>
              <dt class="container-title">ビジョン</dt>
              <dd class="container-text">我々は常に振り返り、努力をし続けることで一番になり、<br>
                憧れられる会社・人物を目指す。<br>
                そして、国・業界をまたいで、<br>
                地域社会から無くてはならない組織であり続ける。</dd>
            </dl>
          </div><!--/.container-inner -->
        </div><!-- /.detaitl -->
      </div><!-- /.wrapper -->
    </section><!-- /.sec01 -->

    <section class="sec02 overview">
      <div class="wrapper">
        <h2 class="overview-title title_obi">会社概要</h2>
        <table class="table">
          <tr>
            <th>法人名</th>
            <td>Dream Japan株式会社</td>
          </tr>
          <tr>
            <th>代表者</th>
            <td>代表取締役　三方　圭一</td>
          </tr>
          <tr>
            <th>設立</th>
            <td>2008年11月</td>
          </tr>
          <tr>
            <th>資本金</th>
            <td>1,000万円</td>
          </tr>
          <tr>
            <th>従業員</th>
            <td>140名</td>
          </tr>
          <tr>
            <th>主要取引先</th>
            <td>(株)スズキ自販京都／京都ダイハツ(株)／他各自動車ディーラー／東京海上日動火災保険(株)／三井住友海上火災保険(株)／(株)損害保険ジャパン日本興亜／あいおいニッセイ同和損害保険(株)／富士火災海上保険(株)</td>
          </tr>
          <tr>
            <th>店舗</th>
            <td>ドリーム加古川本店：兵庫県加古川市米田町船頭字谷107-3<br>
              ドリーム福知山店：京都府福知山市荒河東町100<br>
              ドリーム プレミアム：京都府舞鶴市下福井51<br>
              ドリームMEGA熊本店：熊本県熊本市東区御領8丁目10-101</td>
          </tr>
        </table>
        <ul class="overview-list">
          <li class="overview-list-item">
            <h3 class="title">売上高推移</h3>
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/about/overview_pic01.png" alt=""></p>
          </li>
          <li class="overview-list-item">
            <h3 class="title">車販台数推移</h3>
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/about/overview_pic02.png" alt="車販台数推移"></p>
          </li>
        </ul>
      </div><!-- /.wrapper -->
    </section><!-- /.sec02 -->

    <section class="sec03 history">
      <div class="wrapper">
        <h2 class="history-title title_obi">沿革</h2>
        <dl class="history-inner">
          <dt class="history-title">2008年11月</dt>
          <dd class="history-text">Dream Japan　創業</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2012年8月</dt>
          <dd class="history-text">軽未使用車専門店ドリーム舞鶴店　オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2013年8月</dt>
          <dd class="history-text">軽未使用車専門店ドリーム福知山店　オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2015年5月</dt>
          <dd class="history-text">軽未使用車専門店ドリーム加古川本店　オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2015年10月</dt>
          <dd class="history-text">DREAM JAPAN MALAYSIA SDN.BHD.設立</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2016年2月</dt>
          <dd class="history-text">車検の速太郎加古川店を指定工場としてグランドオープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2016年4月</dt>
          <dd class="history-text">カーコンビニ倶楽部ドリーム舞鶴店オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2017年9月</dt>
          <dd class="history-text">カーコンビニ倶楽部ドリーム加古川店オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2017年10月</dt>
          <dd class="history-text">フラット7ドリーム舞鶴店オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2017年10月</dt>
          <dd class="history-text">軽未使用車専門店ドリーム福知山店　移転オープン</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2018年7月</dt>
          <dd class="history-text">シンガポール・MASHGATE PTE LTD買収</dd>
        </dl>
        <dl class="history-inner">
          <dt class="history-title">2019年3月</dt>
          <dd class="history-text">規模拡大に伴い、本店を加古川に移転</dd>
        </dl>
      </div><!-- /.wrapper -->
    </section><!-- /.sec03 -->

    <section class="sec04 affiliated">
      <div class="wrapper">
        <h2 class="affiliated-title title_obi">系列会社</h2>
        <ul class="affiliated-list">
          <li class="affiliated-list-item">
            <h3 class="affiliated-title">レンタカー</h3>
            <p class="link">
              <a href="https://www.j-netrentacar.co.jp/kyoto/maizuru/" target="_blank">J-netレンタカー</a>
              <span>
                <a href="https://www.instagram.com/jnetkyotokita/" class="insta"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                <!-- <a href="" class="fb"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook" target="_blank"></a> -->
              </span>
            </p>
          </li>
          <li class="affiliated-list-item">
            <h3 class="affiliated-title">飲食事業</h3>
            <p class="link">
              <a href="http://babyface-planets.com/" target="_blank">BABY FACE PLANET’S</a>
              <span>
                <a href="https://www.instagram.com/babyfaceplanetsmaizuruten/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                <!-- <a href="" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a> -->
              </span>
            </p>
          </li>
          <li class="affiliated-list-item">
            <h3 class="affiliated-title">Dream Japan Malaysia</h3>
            <p class="link">
              <a href="" target="_blank">串焼き 中中</a>
              <span>
                <!-- <a href="" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a> -->
                <a href="" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
              </span>
            </p>
            <p class="link" target="_blank">
              <a href="">東京希望軒 JB店</a>
              <span>
                <!-- <a href="" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a> -->
                <a href="https://ja-jp.facebook.com/tokyokiboukenramen/" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
              </span>
            </p>
          </li>
          <li class="affiliated-list-item">
            <h3 class="affiliated-title">MASHGATE PTE LTD</h3>
            <p class="link">
              <a href="http://mashgate.com/">MASHGATE PTE LTD</a>
              <span>
                <a href="https://www.instagram.com/mashgate_singapore/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                <a href="https://www.facebook.com/Mashgate-Pte-Ltd-580763305779346/" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
              </span>
            </p>
          </li>

        </ul>
      </div><!-- /.wrapper -->
    </section><!-- /.sec04 -->

    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
