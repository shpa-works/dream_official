<?php
/*
Template Name: サービス - 車検・整備
*/
?>

<?php
$setPath= "";
$pageTitle = "車検・整備｜サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css">
  </head>

  <body class="service maintenance">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">車検・整備</span><br>
        <span class="en">MAINTENANCE</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/service/"><span itemprop="name">サービス</span></a>
        <meta itemprop="position" content="2">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">車検・整備</span>
        <meta itemprop="position" content="3">
      </li>
    </ol>

    <section class="sec01 point">
      <div class="wrapper">
        <h3 class="point-title title_obi">ドリーム車検が<span>選ばれる理由</span></h3>
        <ol class="point-list">
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/point_pic01.jpg" alt="Point1"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">国土交通省指定の車検工場を完備</dt>
              <dd class="point-list-text">
                <p>ドリームの工場は、国土交通省指定工場を保有しております。<br>
                「指定工場」とは国土交通省から車検の「委託」を受けた工場になります。<br>
                車検整備士は国家資格整備士と自動車検査員を常駐させてお客様のお車を責任を持って車検を行っております。</p>
                <p>もちろん、自社工場ですから土日祝日も車両検査ができる上に、短時間で車検が完了します。</p>
              </dd>
            </dl>
          </li>
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/point_pic02.jpg" alt="Point2"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">便利でお得な短時間車検</dt>
              <dd class="point-list-text">ドリームでは、「短時間車検システム」を採用しています。<br>
                これにより、最短時間で車検を完了させることができます。<br>
                お客様をお待たせせず、車検を完了することができるのです。<br>
                また、このホームページから車検をご予約いただくとお得な割引特典もあります。</dd>
            </dl>
          </li>
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/point_pic03.jpg" alt="Point3"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">地域No.1！の最安値に挑戦します！</dt>
              <dd class="point-list-text">
                <dl class="inner">
                  <dt class="title">無駄な整備をしないから安い！</dt>
                  <dd class="text">車検を通すために必ずやらなければならない項目以外はお客様のご予算やお車に乗車する頻度、年間走行距離などをお聞かせいただいて、ご予算に合う最適な整備を行います。</dd>
                  <dt class="title">部品、工賃が安い！</dt>
                  <dd class="text">ドリーム車検では整備箇所が発生してもできる限りお安くご提供できる仕組みがあります。<br>
                    車検時はお車を分解して点検整備をしているため、作業効率が良くなるので、その分整備料金を安くしてお客様に還元させていただいています。</dd>
                </dl>

              </dd>
            </dl>
          </li>
          <li class="point-list-item">
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/point_pic04.jpg" alt="Point4"></p>
            <dl class="point-list-inner">
              <dt class="point-list-title">車検前の事前見積で安心！</dt>
              <dd class="point-list-text">
                <p>『事前にお客様に詳しいご説明をする』ことを徹底しています。</p>
                <ul class="list">
                  <li class="list-item">整備をしないと車検が通らない部分</li>
                  <li class="list-item">車検を通すのには問題ないが、2年間安全に乗るためには整備したほうがよい部分</li>
                  <li class="list-item">次回の車検まで安心していただける部分</li>
                </ul>
                <p>に分けて、お客様と一緒に立会い点検を行い、ひとつひとつ丁寧に説明いたします。その際、お時間をいただければ事前点検の時点で車を分解し、「事前見積をする」 ことも可能です。<br>
                  ドリーム車検は「地域で一番親切、丁寧な車検」を目指しております。</p>
              </dd>
            </dl>
          </li>
        </ol>
        <div class="color_box_content clearfix">
          <div class="box height">
            <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/discount_num01.png" alt="割引01"></div>
            <div class="tit"><p>インターネット割引</p></div>
            <p class="height02">ホームページよりお申し込みの方は5,000円割引いたします。<span class="note">※他の割引サービスと併用不可</span></p>
            <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/discount_price01.png" alt="修理工賃より10%OFF"></div>
            <dl class="inner">
              <dt class="title"></dt>
              <dd class="shop"></dd>
              <dd class="shop"></dd>
              <dd class="shop"></dd>
            </dl>
          </div>
          <div class="box height">
            <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/discount_num02.png" alt="割引02"></div>
            <div class="tit"><p>ご優待割引</p></div>
            <p class="height02">60才以上の方、身体障害手帳をご持参の方がご一緒にご来店頂けますと、割引させて頂きます。<span class="note">例）お孫さんの車検の際、60才以上の方をお連れ頂いた場合等。</span></p>
            <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/discount_price02.png" alt="修理工賃より1,000円OFF"></div>
            <dl class="inner">
              <dt class="title"></dt>
              <dd class="shop"></dd>
              <dd class="shop"></dd>
              <dd class="shop"></dd>
            </dl>
          </div>
        </div>
      </div><!-- /.wrapper -->
    </section><!-- /.point -->

    <section class="sec02">
      <div class="wrapper">
        <div class="handling">
          <h3 class="handling-title">取扱店舗</h3>
          <ul class="handling-list">
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_kakogawa.jpg" alt="車検の速太郎加古川店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_hayataro.png" alt="車検の速太郎加古川店"><span>加古川店</span></p>
              <p class="add">〒675-0053<br>
                兵庫県加古川市米田町船頭字谷107-3</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0037-6000-3399（通話料無料）</dd>
                <dt>営業時間</dt>
                <dd>10時～20時</dd>
                <dt>定休日</dt>
                <dd>水曜日</dd>
              </dl>
              <p class="link"><a href="https://dreamjapan.jp/kakogawa/" target="_blank">詳しくはこちら</a></p>
            </li>
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_fukuchiyama01.jpg" alt="車検の速太郎福知山店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_hayataro.png" alt="車検の速太郎福知山店"><span>福知山店</span></p>
              <p class="add">〒620-0000<br>
                京都府福知山市篠尾961−44</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0037-6000-3176（通話料無料）</dd>
                <dt>営業時間</dt>
                <dd>10時～20時</dd>
                <dt>定休日</dt>
                <dd>水曜日</dd>
              </dl>
              <p class="link"><a href="https://dreamjapan.jp/hayataro/" target="_blank">詳しくはこちら</a></p>
            </li>
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_maizuru02.jpg" alt="ドリーム車検舞鶴店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_dmsyaken.png" alt="ドリーム車検舞鶴店"><span>舞鶴店</span></p>
              <p class="add">〒624-0946<br>
                京都府舞鶴市下福井51</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0120-762-202（通話料無料）</dd>
                <dt>営業時間</dt>
                <dd>10時～20時</dd>
                <dt>定休日</dt>
                <dd>水曜日</dd>
              </dl>
              <p class="link"><a href="https://maizuru-shaken.com/" target="_blank">詳しくはこちら</a></p>
            </li>
          </ul>
        </div><!-- /.handling -->
        <div class="sec02-button">
          <p><a href="<?php echo home_url(); ?>/reservation/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/maintenance/sec02_button.png" alt="車検のご予約はこちら"></a></p>
        </div>
      </div><!-- /.wrapper -->
    </section><!-- /.sec02 -->

    <section>
      <?php require_once($setPath.'lib/include/service.php'); ?>
    </section>

    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
