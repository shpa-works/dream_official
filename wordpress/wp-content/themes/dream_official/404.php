<?php
/*
Template Name: プライバシーポリシー
*/
?>

<?php
  $setPath= "";
  $pageTitle = "404 not found";
  $pageInfo = array(
    "title" => $pageTitle,
    "keywords" => "",
    "description" => "",
  );
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>


  <body class="notfound">
    <?php require_once('lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">お探しのページが見つかりません</span><br>
        <span class="en">404 NOT FOUND</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">お探しのページが見つかりません</span>
        <meta itemprop="position" content="2">
      </li>
    </ol>

    <section class="sec01">
      <div class="wrapper">
        <p class="notfound-title">404NOT FOUND</p>
        <p class="notfound-text">お探しのページは削除されたか、移動された可能性があります。</p>
      </div><!-- /.wrapper -->
    </section><!-- /.sec01 -->
    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
