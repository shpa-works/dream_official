<table class="form-table">
  <tr>
    <th>お名前<span class="required">【必須】</span></th>
    <td>
      [multiform "your-name"]
    </td>
  </tr>
  <tr>
    <th>フリガナ<span class="required">【必須】</span></th>
    <td>
      [multiform "your-kana"]
    </td>
  </tr>
  <tr>
    <th>電話番号<span class="required">【必須】</span></th>
    <td>
      [multiform "your-phone"]
    </td>
  </tr>
  <tr>
    <th>メールアドレス<span class="required">【必須】</span></th>
    <td>
      [multiform "your-email"]
      <span class="note">※携帯メールアドレスを登録する際に返信をご希望の場合は、パソコンからのメールが受信できるよう<br>
        「メールドメイン指定受信」を解除のうえ送信ください。</span>
    </td>
  </tr>
  <tr>
    <th>お問い合わせ店舗</th>
    <td>
      [multiform "select-store"]
    </td>
  </tr>
  <tr>
    <th>ご来店希望日</th>
    <td>
      [multiform "select-date"]
      [multiform "select-time"]
    </td>
  </tr>
  <tr>
    <th>ご用件<span class="required">【必須】</span></th>
    <td>
      [multiform "select-requirement"]
    </td>
  </tr>
  <tr class="text_area">
    <th>お問い合わせ内容<span class="required">【必須】</span></th>
    <td>
      [multiform "your-nquiries"]
    </td>
  </tr>
</table>
<div class="submit-container">
  <p class="submit-button submit-button-prev">
    [previous]
  </p>
  <p class="submit-button submit-button-submit">
    [submit "送信"]
    [multistep button-send last-step send_email "../contact-thanks/"]
  </p>
</div><!-- /.submit-content -->
