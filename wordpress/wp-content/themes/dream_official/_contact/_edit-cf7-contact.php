<table class="form-table">
  <tr>
    <th>お名前<span class="required">【必須】</span></th>
    <td>
      [text* your-name]
    </td>
  </tr>
  <tr>
    <th>フリガナ<span class="required">【必須】</span></th>
    <td>
      [text* your-kana]
    </td>
  </tr>
  <tr>
    <th>電話番号<span class="required">【必須】</span></th>
    <td>
      [tel* your-phone]
    </td>
  </tr>
  <tr>
    <th>メールアドレス<span class="required">【必須】</span></th>
    <td>
      [email* your-email]
      <span class="note">※携帯メールアドレスを登録する際に返信をご希望の場合は、パソコンからのメールが受信できるよう<br>
        「メールドメイン指定受信」を解除のうえ送信ください。</span>
    </td>
  </tr>
  <tr>
    <th>メールアドレス確認<span class="required">【必須】</span></th>
    <td>
      [email* your-email_confirm]
    </td>
  </tr>
  <tr>
    <th>お問い合わせ店舗</th>
    <td>
      [radio select-store use_label_element default:1 "加古川本店" "福知山店" "舞鶴店" "軽アウトレット店" "熊本店"]
    </td>
  </tr>
  <tr>
    <th>ご来店希望日</th>
    <td class="datetime">
      <span class="input-date">[date select-date]</span>
      <span class="input-time">[text select-time class:timepicker]</span>
    </td>
  </tr>
  <tr>
    <th>ご用件<span class="required">【必須】</span></th>
    <td>
      [radio select-requirement use_label_element default:1 "ご来店予約" "在庫確認" "ローン確認" "オンライン商談" "その他"]
    </td>
  </tr>
  <tr class="text_area">
    <th>お問い合わせ内容<span class="required">【必須】</span></th>
    <td>
      [textarea* your-nquiries]
    </td>
  </tr>
</table>
<div class="consent">
  <p class="consent-text">
    「<a href="../privacypolicy" target"_blank">プライバシーポリシー</a>」をご一読の上、内容に同意いただける場合は「同意する」にチェックし、内容確認画面にお進みください。
  </p>
  <p class="consent-button">
    [checkbox* check-pp use_label_element "同意する"]
  </p>

</div><!-- /.consent -->
<div class="submit-container">
  <p class="submit-button submit-button-confirm">
    [submit "確認画面へ"]
    [multistep button-confirm first_step "https://dream-official.shpatest.com/contact-confirm/"]
  </p>
</div><!-- /.submit-content -->
