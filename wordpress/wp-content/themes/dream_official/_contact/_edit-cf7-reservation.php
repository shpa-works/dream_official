<table class="form-table">
  <tr>
    <th>ご予約店舗<span class="required">【必須】</span></th>
    <td>
      [multiform "select-store"]
    </td>
  </tr>
  <tr>
    <th>ご予約希望日<span class="required">【必須】</span></th>
    <td>
      <ol class="date-list">
        <li class="date-list-item">
          第1希望<br>
          [multiform "select-date_first"]
          [multiform "select-time_first"]
        </li>
        <li class="date-list-item">
          第2希望<br>
          [multiform "select-date_second"]
          [multiform "select-time_second"]
        </li>
        <li class="date-list-item">
          第3希望<br>
          [multiform "select-date_third"]
          [multiform "select-time_third"]
        </li>
      </ol>
    </td>
  </tr>
  <tr>
    <th>お名前<span class="required">【必須】</span></th>
    <td>
      [multiform "your-name"]
    </td>
  </tr>
  <tr>
    <th>フリガナ<span class="required">【必須】</span></th>
    <td>
      [multiform "your-kana"]
    </td>
  </tr>
  <tr>
    <th>電話番号</th>
    <td>
      [multiform "your-phone"]
    </td>
  </tr>
  <tr>
    <th>メールアドレス<span class="required">【必須】</span></th>
    <td>
      [multiform "your-email"]
      <span class="note">※携帯メールアドレスを登録する際に返信をご希望の場合は、パソコンからのメールが受信できるよう<br>
        「メールドメイン指定受信」を解除のうえ送信ください。</span>
    </td>
  </tr>
  <tr>
    <th>お車のプレート番号<span class="required">【必須】</span></th>
    <td>
      [multiform "your-numberplate"]
    </td>
  </tr>
  <tr>
    <th>車種名<span class="required">【必須】</span></th>
    <td>
      [multiform "your-cartype"]
    </td>
  </tr>
  <tr>
    <th>車検満了日</th>
    <td>
      [multiform "expiration-date"]
    </td>
  </tr>
  <tr class="text_area">
    <th>ご質問・ご要望</th>
    <td>
      [multiform "your-nquiries"]
    </td>
  </tr>
</table>
<div class="submit-container">
  <p class="submit-button submit-button-prev">
    [previous]
  </p>
  <p class="submit-button submit-button-submit">
    [submit "送信"]
    [multistep button-send last-step send_email "https://dream-official.shpatest.com/reservation-thanks/"]
  </p>
</div><!-- /.submit-content -->
