<table class="form-table">
  <tr>
    <th>ご予約店舗<span class="required">【必須】</span></th>
    <td>
      <input type="radio" id="" name="お問い合わせ店舗" checked><span>車検の速太郎加古川店</span>
      <input type="radio" id="" name="お問い合わせ店舗"><span>車検の速太郎福知山店</span>
      <input type="radio" id="" name="お問い合わせ店舗"><span>ドリーム車検舞鶴店</span>
    </td>
  </tr>
  <tr>
    <th>ご予約希望日<span class="required">【必須】</span></th>
    <td>
      <ol class="date-list">
        <li class="date-list-item">
          第1希望<br>
          [date select-date_first]
          <input type="time" name="select-time_first">
        </li>
        <li class="date-list-item">
          第2希望<br>
          [date select-date_second]
          <input type="time" name="select-time_second">
        </li>
        <li class="date-list-item">
          第3希望<br>
          [date select-date_third]
          <input type="time" name="select-time_third">
        </li>
      </ol>
    </td>
  </tr>
	<tr>
    <th>お名前<span class="required">【必須】</span></th>
    <td>
      [text* your-name]
    </td>
	</tr>
  <tr>
    <th>フリガナ<span class="required">【必須】</span></th>
    <td>
      [text* your-kana]
    </td>
	</tr>
  <tr>
    <th>電話番号</th>
    <td>
      [tel* your-phone]
    </td>
	</tr>
  <tr>
    <th>メールアドレス<span class="required">【必須】</span></th>
    <td>
      [email* your-email]
      <span class="note">※携帯メールアドレスを登録する際に返信をご希望の場合は、パソコンからのメールが受信できるよう<br>
        「メールドメイン指定受信」を解除のうえ送信ください。</span>
    </td>
	</tr>
  <tr>
    <th>メールアドレス確認<span class="required">【必須】</span></th>
    <td>
      [email* your-email_confirm]
    </td>
	</tr>

  <tr>
    <th>車検満了日</th>
    <td>
      [date expiration-date]
    </td>
  </tr>
  <tr class="text_area">
    <th>ご質問・ご要望</th>
    <td>
      [textarea* your-nquiries]
    </td>
  </tr>
</table>
<div class="consent">
  <p class="consent-text">
    「<a href="<?php home_url(); ?>/privacypolicy" target"_blank">プライバシーポリシー</a>」をご一読の上、内容に同意いただける場合は「同意する」にチェックし、内容確認画面にお進みください。
  </p>
  <p class="consent-button">
    [checkbox* check-pp use_label_element "同意する"]
  </p>
</div><!-- /.consent -->
<div class="submit-container">
  <p class="submit-button submit-button-prev">
    [previous]
  </p>
  <p class="submit-button submit-button-submit">
    [submit "送信"]
    [multistep button-send last-step send_email "../thanks/"]
  </p>
</div><!-- /.submit-content -->
