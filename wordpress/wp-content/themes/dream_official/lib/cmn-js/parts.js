/*=============================================
　　PAGE TOP
==============================================*/
$(function () {
    var topBtn = $('#page-top');
    topBtn.hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });

    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});


/*=============================================
　　SCROLL
==============================================*/
$(function(){
  $('a[href^="#"]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
});


/*=============================================
　　VIEWPORT
==============================================*/
$(function () {
    var ua = {
        iPhone: navigator.userAgent.indexOf('iPhone') != -1,
        iPad: navigator.userAgent.indexOf('iPad') != -1,
        iPod: navigator.userAgent.indexOf('iPod') != -1,
        android: navigator.userAgent.indexOf('Android') != -1,
        windows: navigator.userAgent.indexOf('Windows Phone') != -1
    }
    if (ua.iPhone || ua.iPod || ua.android || ua.windows) {
    } else {
    }
}());


/*=============================================
　　MENU
==============================================*/
/*SP MENU*/
$(function () {
  var $body = $('body');
  $('#navbtn').on('click', function () {
      $body.toggleClass('open');
  });
  $('.gnav a').on('click', function () {
      $body.removeClass('open');
  });
});


function acdMenu() {
  //デフォルトでアコーディオンの中身を非表示
  $(".acMenu .menu-link").css("display", "none");
  $(".acMenu2 ul").css("display", "none");

  //第2階層のアコーディオン　
  $(".acMenu p").click(function() {
      $(".acMenu p").not(this).next().slideUp("fast");
      $(this).toggleClass("open").next().slideToggle("fast");
  });
}

$(function() {
  acdMenu();
});


/*=============================================
　　MATCH HEIGHT nomal
==============================================*/
$(function () {
    $('.height').matchHeight({property: 'min-height'});
});
$(function () {
    $('.height02').matchHeight({property: 'min-height'});
});


/*=============================================
　　HEADER SHADOW
==============================================*/
$(window).scroll(function(){
  var element = $('header'),
       scroll = $(window).scrollTop(),
       height = element.outerHeight();
  if ( scroll > height ) {
    element.addClass('shadow');
  } else {
    element.removeClass('shadow');
  }
});


/*=============================================
　　TAB SWITCH
==============================================*/
document.addEventListener('DOMContentLoaded', function(){
  const tabs = document.getElementsByClassName('tab');
  for(let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', tabSwitch);
  }
  function tabSwitch(){
    document.getElementsByClassName('is-active')[0].classList.remove('is-active');
    this.classList.add('is-active');
    document.getElementsByClassName('is-show_panel')[0].classList.remove('is-show_panel');
    const arrayTabs = Array.prototype.slice.call(tabs);
    const index = arrayTabs.indexOf(this);
    document.getElementsByClassName('panel')[index].classList.add('is-show_panel');
  };
});