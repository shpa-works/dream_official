<div class="fixed_menu">
  <ul class="clearfix">
    <li><a href="<?php echo home_url(); ?>/contact/">ご来店<br>予約</a></li>
    <li><a href="<?php echo home_url(); ?>/reservation/">車検<br>ご予約</a></li>
    <li><a href="https://bankin.dreamjapan.jp/kakogawa/bankin/" target="_blank">鈑⾦<br>ご予約</a></li>
  </ul>
</div>

<header class="header">
  <div class="head_wrap clearfix">
    <div class="head_con01 clearfix">
      <h1 class="logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/logo.png" alt="ドリームジャパン"></a></h1>
      <div class="beginners_btn"><a href="<?php home_url(); ?>/dream/"><span>はじめての方へ</span></a></div>
    </div>

    <div class="head_con02">
      <nav class="global_menu">
        <ul class="clearfix">
          <li class="menu"><a href="<?php echo home_url(); ?>/dream/">ドリームの魅力</a></li>
          <li class="menu"><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
          <li class="menu bg">
            <a href="<?php echo home_url(); ?>/service/">サービス</a>
            <div class="child_menu">
              <ul>
                <li>
                  <a href="<?php echo home_url(); ?>/service/unused/">
                    <div class="img"></div>
                    <p>未使用車販売</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/service/maintenance/">
                    <div class="img"></div>
                    <p>車検・整備</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/service/repair/">
                    <div class="img"></div>
                    <p>鈑金・修理</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/service/insurance/">
                    <div class="img"></div>
                    <p>保険</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/service/carrental/">
                    <div class="img"></div>
                    <p>レンタカー</p>
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/service/flatseven/">
                    <div class="img"></div>
                    <p>フラット7</p>
                  </a>
                </li>
                <li>
                  <a href="https://k-398.dreamjapan.jp/" target="_blank">
                    <div class="img"></div>
                    <p>中古車販売39.8</p>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="menu"><a href="<?php echo home_url(); ?>/information/">店舗紹介</a></li>
        </ul>
      </nav>
      <div class="contact_btn">
        <a href="<?php echo home_url(); ?>/contact/"><span>お問い合わせ</span></a>
      </div>
    </div>
  </div>
  <button type="button" id="navbtn" class="navbtn"></button>
</header>

<nav class="gnav sp-on">
  <div class="wrapper">
    <ul class="acMenu">
      <li>
        <p class="menu-ttl"><a href="<?php echo home_url(); ?>/dream/">ドリームの魅力</a></p>
      </li>
      <li>
        <p class="menu-ttl"><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></p>
      </li>
      <li>
        <p class="menu-ttl"><span>サービス</span></p>
        <ul class="acMenu2 menu-link">
          <li><a href="<?php echo home_url(); ?>/service/unused/">未使⽤⾞販売</a></li>
          <li><a href="<?php echo home_url(); ?>/service/maintenance/">⾞検・整備</a></li>
          <li><a href="<?php echo home_url(); ?>/service/repair/">鈑⾦・修理</a></li>
          <li><a href="<?php echo home_url(); ?>/service/insurance/">保険</a></li>
          <li><a href="<?php echo home_url(); ?>/service/carrental/">レンタカー</a></li>
          <li><a href="<?php echo home_url(); ?>/service/flatseven/">フラット7</a></li>
          <li><a href="https://k-398.dreamjapan.jp/" target="_blank">中古⾞販売39.8</a></li>
        </ul>
      </li>
      <li>
        <p class="menu-ttl"><a href="<?php echo home_url(); ?>/information/">店舗紹介</a></p>
      </li>
    </ul>
    <div class="contact_btn">
      <a href="<?php echo home_url(); ?>/contact/"><span>お問い合わせ</span></a>
    </div>
  </div>
</nav>
