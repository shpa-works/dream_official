<?php
  if (empty($pageInfo["title"])) {
    $title = "ドリーム・軽未使用車専門店｜加古川・福知山・舞鶴最大級1000台在庫";
  } else {
    $title = $pageInfo["title"] . "｜ ドリーム・軽未使用車専門店｜加古川・福知山・舞鶴最大級1000台在庫";
  }

  if (empty($pageInfo["keywords"])) {
    $keywords = "軽自動車,未使用車,新古車,中古車,加古川,福知山,舞鶴,兵庫,京都北部,スズキ,ダイハツ,ホンダ,三菱,日産";
  } else {
    $keywords = $pageInfo["keywords"];
  }

  if (empty($pageInfo["description"])) {
    $description = "加古川・福知山・舞鶴で軽自動車の未使用車・新古車のことなら地域最大級の軽自動車専門店ドリームにお任せください！スズキ・ダイハツ・ホンダ・三菱・日産等の最新軽自動車を常時1000台以上在庫しております。加古川・福知山・舞鶴で軽自動車を買うならドリームにお越しください。";
  } else {
    $description = $pageInfo["description"];
  }
?>

<?php //require_once(echo get_template_directory_uri().'/lib/include/google-analytics.php'); ?>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title;?></title>
  <meta name="format-detection" content="telephone=no,email=no">
  <meta name="keywords" content="<?php echo $keywords; ?>">
  <meta name="description" content="<?php echo $description; ?>">
  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/lib/favicon/favicon.ico" type="image/vnd.microsoft.icon">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/lib/favicon/favicon.ico">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/lib/cmn-css/common.css" media="all">
<?php wp_head(); ?>
