<div class="service">
  <div class="wrapper">
    <div class="title_wrap01">
      <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/service_title.png" alt="Dreamのサービス"></h2>
      <div class="en_tit"><p>SERVICES</p></div>
    </div>
    <div class="mintit">
      <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/service_title02.png" alt="お⾞のことならなんでもお任せ！">
      <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/sp/service_title02.png" alt="お⾞のことならなんでもお任せ！">
    </div>

    <div class="holder">
      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 01</span></p>
          <div class="txt_cont">
            <p class="tit">未使⽤⾞販売</p>
            <p class="txt">軽⾃動⾞・コンパクトカー未使⽤⾞専⾨店</p>
            <div class="btn"><a href="<?php home_url(); ?>/service/unused/">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>

      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 02</span></p>
          <div class="txt_cont">
            <p class="tit">⾞検・整備</p>
            <p class="txt">最短45分＆地域最安値に挑戦</p>
            <div class="btn"><a href="<?php home_url(); ?>/service/maintenance/">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>

      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 03</span></p>
          <div class="txt_cont">
            <p class="tit">鈑⾦・修理</p>
            <p class="txt">⼩キズから、⼤キズまで確かな技術で修復！</p>
            <div class="btn"><a href="<?php home_url(); ?>/service/repair/">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>

      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 04</span></p>
          <div class="txt_cont">
            <p class="tit">保険</p>
            <p class="txt">⾃動⾞保険もドリームにお任せ</p>
            <div class="btn"><a href="<?php home_url(); ?>/service/insurance/">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>

      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 05</span></p>
          <div class="txt_cont">
            <p class="tit">レンタカー</p>
            <p class="txt">個⼈のお客様も法⼈様も！</p>
            <div class="btn"><a href="<?php home_url(); ?>/service/carrental/">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>

      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 06</span></p>
          <div class="txt_cont">
            <p class="tit">フラット7</p>
            <p class="txt">新⾞の軽⾃動⾞が⽉々1.1万円(税込)で乗れる!!</p>
            <div class="btn"><a href="<?php home_url(); ?>/service/flatseven/">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>

      <div class="box clearfix">
        <div class="txt_box height">
          <p class="number"><span>SERVICE 07</span></p>
          <div class="txt_cont">
            <p class="tit">中古⾞販売39.8</p>
            <p class="txt">予算50万円で中古⾞をお考えの⽅はこちら</p>
            <div class="btn"><a href="https://k-398.dreamjapan.jp/" target="_blank">詳しく見る</a></div>
          </div>
        </div>
        <div class="img_box height">&nbsp;</div>
      </div>
    </div>

    <div class="button"><a href="<?php home_url(); ?>/service/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/service_btn.png" alt="ドリームのサービスを詳しく見る"></a></div>
  </div>
</div>
