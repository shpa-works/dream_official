<section class="footer_bnr">
  <div class="wrapper">
    <div class="holder clearfix">
      <div class="bnr"><a target="_blank" href="https://shaken.dreamjapan.jp/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn01.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://dreamjapan.jp/kakogawa/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn02.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://dreamjapan.jp/hayataro/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn03.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://bankin.dreamjapan.jp/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn04.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://bankin.dreamjapan.jp/kakogawa/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn05.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://www.j-netrentacar.co.jp/kyoto/maizuru/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn06.png" alt=""></a></div>
      <div class="bnr"><a target="_blank" href="https://kyoto-brc.com/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_btn07.png" alt=""></a></div>
    </div>
  </div>
</section>

<section class="footer_info">
  <div class="wrapper">
    <div class="holder clearfix">
      <div class="box height">
        <p class="title"><span>ドリーム加古川本店</span></p>
        <div class="phone">
          <a href="tel:079-434-3000">079-434-3000</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒675-0053<br>兵庫県加古川市⽶⽥町船頭字⾕107-3</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>第3水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream_kakogawa?share" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリーム福知⼭店</span></p>
        <div class="phone">
          <a href="tel:0773-23-0031">0773-23-0031</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒620-0000<br>京都府福知山市篠尾961−44</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream-fukushiyama?share" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリーム プレミアム</span></p>
        <div class="phone">
          <a href="tel:0773-78-3399">0773-78-3399</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒624-0946<br>京都府舞鶴市下福井51</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/dream_maizuru?share" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>軽アウトレット専⾨店</span></p>
        <div class="phone">
          <a href="tel:0773-25-0398">0773-25-0398</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒620-0061<br>京都府福知⼭市荒河東町100</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <div class="map_icon"><a href="https://g.page/keidream-outlet?share "target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>

      <div class="box height">
        <p class="title"><span>ドリームMEGA熊本店</span></p>
        <div class="phone">
          <a href="tel:096-349-0100">096-349-0100</a>
        </div>
        <div class="info clearfix">
          <div class="txt_box">
            <p class="address">〒861-8035<br>熊本県熊本市東区御領8丁目10-101</p>
            <dl>
              <dt>営業時間</dt>
              <dd>10時〜20時</dd>
              <dt>定休⽇</dt>
              <dd>水曜日</dd>
            </dl>
          </div>
          <!-- <div class="map_icon"><a href="https://goo.gl/maps/sFt6iTTRJG5QhPeG8" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div> -->
          <div class="map_icon"><a href="https://goo.gl/maps/SYKEAYZhWngSR8g26" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/ico-map.png" alt="MAP"></a></div>
        </div>
      </div>
    </div>
  </div>
</section>

<footer>
  <div class="footer_cont clearfix">
    <div class="footer_logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/footer_logo.png" alt="ドリームジャパン"></a></div>
    <div class="nav">
      <ul class="bloc">
        <li><a href="<?php echo home_url(); ?>/dream/">ドリームの魅力</a></li>
        <li><a href="<?php echo home_url(); ?>/news/">新着情報</a></li>
        <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
      </ul>
      <ul class="bloc">
        <li>
          <a href="<?php echo home_url(); ?>/service/">サービス</a>
          <ul class="clearfix">
            <li><a href="<?php echo home_url(); ?>/service/unused/">未使用車販売</a></li>
            <li><a href="<?php echo home_url(); ?>/service/maintenance/">車検・整備</a></li>
            <li><a href="<?php echo home_url(); ?>/service/repair/">鈑金・修理</a></li>
            <li><a href="<?php echo home_url(); ?>/service/insurance/">保険</a></li>
            <li><a href="<?php echo home_url(); ?>/service/carrental/">レンタカー</a></li>
            <li><a href="<?php echo home_url(); ?>/service/flatseven/">フラット7</a></li>
            <li><a href="https://k-398.dreamjapan.jp/" target="_blank">中古車販売39.8</a></li>
          </ul>
        </li>
      </ul>
      <ul class="bloc">
        <li><a href="<?php echo home_url(); ?>/information/">店舗紹介</a></li>
        <li><a href="<?php echo home_url(); ?>/news/">スタッフブログ</a></li>
      </ul>
      <ul class="bloc">
        <li><a href="<?php echo home_url(); ?>/about/">会社案内</a></li>
        <li><a href="<?php echo home_url(); ?>/contact/">お問い合わせ</a></li>
        <li><a href="<?php echo home_url(); ?>/privacypolicy/">個人情報保護方針</a></li>
      </ul>
    </div>
    <ul class="nav_sp">
      <li><a href="<?php echo home_url(); ?>/dream/">ドリームの魅力</a></li>
      <li><a href="<?php echo home_url(); ?>/service/">サービス</a></li>
      <li><a href="<?php echo home_url(); ?>/news/">新着情報</a></li>
      <li><a href="https://zaiko.dreamjapan.jp/usedcar/">最新在庫情報</a></li>
      <li><a href="<?php echo home_url(); ?>/information/">店舗紹介</a></li>
      <li><a href="<?php echo home_url(); ?>/news/">スタッフブログ</a></li>
      <li><a href="<?php echo home_url(); ?>/information">会社案内</a></li>
      <li><a href="<?php echo home_url(); ?>/privacypolicy/">個人情報保護方針</a></li>
    </ul>
    <div class="contact_btn sp-on">
      <a href="<?php echo home_url(); ?>/contact/"><span>お問い合わせ</span></a>
    </div>
  </div>
  <div class="copyright"><p>©2022 Dream Japan.co,.ltd All Rights Reserved.</p></div>
</footer>

<!--<div id="page-top"><a href="#"><img src="/lib/cmn-img/common/pagetop.jpg"></a></div>-->
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri(); ?>/lib/cmn-js/all.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/lib/cmn-js/parts.js"></script>
<?php wp_footer(); ?>
