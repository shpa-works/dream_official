<?
/*
Template Name: さんぷる
*/
?>
<!DOCTYPE html>
<html lang="ja">

<head>
<?php get_header(); ?>

<?php
if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="entry"><?php the_content(); ?></div>

<?php endwhile; else: ?>

<p>記事が見つかりませんでした。</p>

<?php endif; ?>

<?php get_footer(); ?>
