<?php
/*
Template Name: サービス - トップ
*/
?>

<?php
$setPath= "";
$pageTitle = "サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="service index">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <div id="keyVisual">
    <h2 class="page_title">
      <span class="jp">サービス</span><br>
      <span class="en">SERVICE</span>
    </h2>
  </div>

  <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
      <meta itemprop="position" content="1">
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <span itemprop="name">サービス</span>
      <meta itemprop="position" content="2">
    </li>
  </ol>

  <section class="sec01">
    <div class="wrapper">
      <div class="top_txt">
        <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/index/sec01_txt.png" alt="「車が壊れたらどうしたらいいの？」"></div>
        <p class="txt">クルマの事ってわからないことだらけ。<br>でも、ドリームなら車のことはぜ～んぶおまかせ！わからないことがあればすぐにドリームにご連絡を！<br>クルマのことならいつでもあなたをサポートします！</p>
      </div>

      <div class="service_img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/index/sec01_img.png" alt="Dreamのサービス"></div>
    </div>
  </section>

  <section>
    <?php require_once($setPath.'lib/include/service.php'); ?>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
</body>
</html>
