<?php
/*
Template Name: サービス - 保険
*/
?>

<?php
$setPath= "";
$pageTitle = "保険｜サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="service insurance">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">保険</span><br>
        <span class="en">INSURANCE</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/service/"><span itemprop="name">サービス</span></a>
        <meta itemprop="position" content="2">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">保険</span>
        <meta itemprop="position" content="3">
      </li>
    </ol>

    <section class="sec01">
      <div class="wrapper">
        <h2 class="sec01-title">
          <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/insurance/sec01_title.png" alt="車が壊れたらどうすればいいの？事故の時ってどうするの？">
        </h2>
        <p class="text">万が一の事故の時も、保険の手続きから愛車の修理まで<br class="sp-on">当社がトータルサポート致します。</p>
        <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/insurance/sec01_pic.png" alt="ドリームなら電話１本でOK！"></p>
      </div><!-- /.wrapper -->
    </section><!-- /.sec01 -->

    <section class="sec02">
      <div class="wrapper">
        <h2 class="sec02-title title_obi">取扱保険会社</h2>
        <ul class="sec02-list">
          <li class="sec02-list-item">
            <p class="title">5年連続<br>
              フロンティア/最優績代理店</p>
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/insurance/sec02_pic01.png" alt="東京海上日動"></p>
          </li>
          <li class="sec02-list-item">
            <p class="title">3年連続<br>
              自動車保険全国1位</p>
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/insurance/sec02_pic02.png" alt="損保ジャパン日本興亜"></p>
          </li>
          <li class="sec02-list-item">
            <p class="title">ディーラー<br>
              特級認証代理店</p>
            <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/insurance/sec02_pic03.png" alt="三井住友海上"></p>
          </li>
        </ul>
        <dl class="policy-inner">
          <dt class="policy-title">損害保険代理店ポリシー</dt>
          <dd class="policy-text">不十分不適切な自動車保険契約は意外に多く、<br class="pc-on">
            それらはすべて契約者であるお客様の不利益となります。<br>
            私どもは、損害保険代理店として、お客様の安全で安心できるカーライフのために<br class="pc-on">
            自動車保険の証券内容の確認とアドバイスを行っています。<br>
            これらは自動車保険の契約をお願いするものではありません。</dd>
        </dl>
        <p class="sec02-button"><a href="<?php home_url(); ?>/contact/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/insurance/sec02_button.png" alt="保険の乗り換えを相談する"></a></p>
      </div><!-- /.wrapper -->
    </section><!-- /.sec02 -->


    <section>
      <?php require_once($setPath.'lib/include/service.php'); ?>
    </section>

    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
