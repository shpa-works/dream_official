<?php
  $setPath= "";
  $pageTitle = "";
  $pageInfo = array(
    "title" => $pageTitle,
    "keywords" => "",
    "description" => "",
  );
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.css" integrity="sha512-LVOzFPLcBUppn3NOx8FXJkh2TawWu/jk9ynbntKID6cjynQsfqmHlUbH72mjAwZXsu0LOLw26JoiC0qHJde70Q==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" integrity="sha512-jRxwiuoe3nt8lMSnOzNEuQ7ckDrLl31dwVYFWS6jklXQ6Nzl7b05rrWF9gjSxgOow5nFerdoN6CBB4gY5m5nDw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body class="home">
  <?php require_once('lib/include/header.php'); ?>

  <section class="sec01">
    <div class="wrapper">
      <div class="mv">
        <video src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/video/top_movie_base.mp4" poster="" width="100%" loop autoplay muted playsinline ></video>
        <!-- <video class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/video/top_movie_base_sp.mp4" poster="" width="100%" loop autoplay muted playsinline ></video> -->
      </div>

      <div class="present_bnr">
        <a href="<?php echo home_url(); ?>/coupon/form.php">
          <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_present01.jpg" alt="プレゼント">
          <!-- <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_present01.jpg" alt="プレゼント"> -->
          <!-- <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_present01.jpg" alt="プレゼント"> -->
        </a>
      </div>
      <div class="present_bnr">
        <a href="https://zaiko.dreamjapan.jp/usedcar/usedcar_search.php?maker_list=00300&model_code=00400&price_from=-1&price_to=-1&kyoten_list=-1&hdn_sort_code=1&hdn_sort_kbn=1&hdn_selected_page=&keitora=-1&type=">
          <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_n-box.jpg" alt="N-BOX">
          <!-- <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_present01.jpg" alt="プレゼント"> -->
          <!-- <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_present01.jpg" alt="プレゼント"> -->
        </a>
      </div>
    </div>
  </section>


  <section class="sec02">
    <div class="wrapper">
      <ul class="nav_area clearfix">
        <li><a href="<?php echo home_url(); ?>/service/unused/">未使用車とは</a></li>
        <li><a href="#flier">今週のチラシ</a></li>
        <li><a href="#medama">今週の目玉車</a></li>
        <li><a href="https://zaiko.dreamjapan.jp/usedcar/">在庫を見る</a></li>
      </ul>

      <div class="medama" id="medama">
        <div class="title_wrap01">
          <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/medama_title.png" alt="今週の目玉車"></h2>
          <div class="en_tit"><p>THIS WEEK’S SPECIAL CAR</p></div>
        </div>
        <div class="white_board">
          <iframe type="text/html" frameborder="0" width="100%" height="1570px" src="https://zaiko.dreamjapan.jp/usedcar/usedcar_search_recommend.php" ></iframe>
        </div>
      </div>

      <div class="body_type">
        <div class="title_wrap01">
          <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/body_type_title.png" alt="ボディタイプで選ぶ"></h2>
          <div class="en_tit"><p>CHOOSE BY BODY TYPE</p></div>
        </div>

        <div class="holder clearfix">
          <div class="box">
            <div class="img_box height"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/body_type_slide.png" alt="スライド"></div>
            <div class="txt_box">
              <a href="https://zaiko.dreamjapan.jp/usedcar/usedcar_search.php?type=3">スライド</a>
            </div>
          </div>
          <div class="box">
            <div class="img_box height"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/body_type_sedan.png" alt="セダン"></div>
            <div class="txt_box">
              <a href="https://zaiko.dreamjapan.jp/usedcar/usedcar_search.php?type=1">セダン</a>
            </div>
          </div>
          <div class="box">
            <div class="img_box height"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/body_type_wagon.png" alt="ワゴン"></div>
            <div class="txt_box">
              <a href="https://zaiko.dreamjapan.jp/usedcar/usedcar_search.php?type=2">ワゴン</a>
            </div>
          </div>
          <div class="box">
            <div class="img_box height"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/body_type_suv.png" alt="SUV"></div>
            <div class="txt_box">
              <a href="https://zaiko.dreamjapan.jp/usedcar/usedcar_search.php?type=4">SUV</a>
            </div>
          </div>
          <div class="box">
            <div class="img_box height"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/body_type_ban.png" alt="バントラ"></div>
            <div class="txt_box">
              <a href="https://zaiko.dreamjapan.jp/usedcar/usedcar_search.php?keitora=1">バントラ</a>
            </div>
          </div>
        </div>
      </div>

      <div class="stock_bnr">
        <a href="https://zaiko.dreamjapan.jp/usedcar/">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_stock_check.png" alt="最新在庫情報をチェック！">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_stock_check.png" alt="最新在庫情報をチェック！">
        </a>
      </div>
    </div>
  </section>

  <section class="sec03" id="flier">
    <div class="tit_con">
      <div class="wrapper">
        <div class="title_wrap01">
          <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/chirashi_title.png" alt="今週のチラシ"></h2>
          <div class="en_tit"><p>THIS WEEK’S FLIER</p></div>
        </div>
      </div>
    </div>
    <div class="content">
      <div class="wrapper">
        <div class="holder clearfix">
          <div class="box">
            <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kakogawa_omote.jpg" alt="チラシ"></div>
            <div class="btn"><a href="#modal"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/chirashi_btn01.png" alt="CLICK!"></a></div>
            <div class="remodal" data-remodal-id="modal">
              <p class="remodal-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_kakogawa_omote.jpg" alt="チラシ"></p>
            </div><!-- /.remodal -->
          </div>
          <div class="box">
            <div class="chirashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_fukuchiyama_omote.jpg" alt="チラシ"></div>
            <div class="btn"><a href="#modal"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/chirashi_btn01.png" alt="CLICK!"></a></div>
            <div class="remodal" data-remodal-id="modal">
              <p class="remoda-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/deal/chirashi/chirashi_fukuchiyama_omote.jpg" alt="チラシ"></p>
            </div><!-- /.remodal -->
          </div>
        </div>
        <div class="button"><a href="<?php home_url(); ?>/deal/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/chirashi_btn02.png" alt="お得な情報をもっと見る"></a></div>
      </div>
    </div>
  </section>


  <section class="sec04">
    <div class="wrapper">
      <div class="title_wrap01">
        <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_title.png" alt="人気車種ランキング"></h2>
        <div class="en_tit"><p>POPULAR CAR MODEL RANKING</p></div>
      </div>

      <div class="tab-panel">
        <ul class="tab-group">
          <li class="tab tab-A is-active"><span>セダン</span></li><!--
          --><li class="tab tab-B"><span>ワゴン</span></li><!--
          --><li class="tab tab-C"><span>スライド</span></li><!--
          --><li class="tab tab-D"><span>SUV</span></li>
        </ul>

        <div class="panel-group">
          <div class="panel tab-A is-show_panel clearfix">
            <div class="holder clearfix">
              <div class="box rank01 sp-on"><!----- Rank01 SP用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_es.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">DAIHATSU</p>
                    <p class="syasyu">ミライース</p>
                  </div>
                  <p class="txt">取り回しの良いボディサイズで狭い道やカーブでもノンストレスな走行が可能です。</p>
                </div>
              </div>
              <div class="box rank02">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_lapin.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts02.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">ラパン</p>
                  </div>
                  <p class="txt">可愛い見た目が特徴で、特に女性に人気の１台です。</p>
                </div>
              </div>
              <div class="box rank01 pc-on"><!----- Rank01 PC用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_es.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">DAIHATSU</p>
                    <p class="syasyu">ミライース</p>
                  </div>
                  <p class="txt">取り回しの良いボディサイズで狭い道やカーブでもノンストレスな走行が可能です。</p>
                </div>
              </div>
              <div class="box rank03">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_alto.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts03.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">アルト</p>
                  </div>
                  <p class="txt">実用性と経済性を兼ね備えたロングセラーの車種でインテリアも特徴的です。</p>
                </div>
              </div>
            </div>
          </div><!------- tab-A ------->

          <div class="panel tab-B clearfix">
            <div class="holder clearfix">
              <div class="box rank01 sp-on"><!----- Rank01 SP用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_wagonr.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">ワゴンR</p>
                  </div>
                  <p class="txt">軽のハイトワゴン車のパイオニア。広々とした室内スペースが特徴的です。</p>
                </div>
              </div>
              <div class="box rank02">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_move.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts02.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">DAIHATSU</p>
                    <p class="syasyu">ムーブ</p>
                  </div>
                  <p class="txt">嬉しい機能が詰まったワゴンタイプ。大人が４人乗っても余裕のある室内スペースが特徴です。</p>
                </div>
              </div>
              <div class="box rank01 pc-on"><!----- Rank01 PC用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_wagonr.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">ワゴンR</p>
                  </div>
                  <p class="txt">軽のハイトワゴン車のパイオニア。広々とした室内スペースが特徴的です。</p>
                </div>
              </div>
              <div class="box rank03">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_days.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts03.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">NISSAN</p>
                    <p class="syasyu">デイズ</p>
                  </div>
                  <p class="txt">軽自動車という枠を超えた広い室内と荷室を完備。さらに最新モデルには多数の先進技術が搭載されています。</p>
                </div>
              </div>
            </div>
          </div><!------- tab-B ------->

          <div class="panel tab-C clearfix">
            <div class="holder clearfix">
              <div class="box rank01 sp-on"><!----- Rank01 SP用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_nbox.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">HONDA</p>
                    <p class="syasyu">N-BOX</p>
                  </div>
                  <p class="txt">不動の軽自動車人気No.１車種。デザイン性、機能性において高い評価を得ています。</p>
                </div>
              </div>
              <div class="box rank02">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_spacia.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts02.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">スペーシア</p>
                  </div>
                  <p class="txt">豊富なカラーバリエーションとスライドドアをはじめとした高い機能性でファミリーカーとして人気です。</p>
                </div>
              </div>
              <div class="box rank01 pc-on"><!----- Rank01 PC用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_nbox.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">HONDA</p>
                    <p class="syasyu">N-BOX</p>
                  </div>
                  <p class="txt">不動の軽自動車人気No.１車種。デザイン性、機能性において高い評価を得ています。</p>
                </div>
              </div>
              <div class="box rank03">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_tanto.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts03.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">DAIHATSU</p>
                    <p class="syasyu">タント</p>
                  </div>
                  <p class="txt">車高が高く、室内スペースが広くデザインされた１台で、小さなお子さんのいるお客様に人気です。</p>
                </div>
              </div>
            </div>
          </div><!------- tab-C ------->

          <div class="panel tab-D clearfix">
            <div class="holder clearfix">
              <div class="box rank01 sp-on"><!----- Rank01 SP用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_hustler.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">ハスラー</p>
                  </div>
                  <p class="txt">まさに「遊べる軽自動車」。アウトドアにも街乗りにもおすすめです。</p>
                </div>
              </div>
              <div class="box rank02">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_taft.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts02.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">DAIHATSU</p>
                    <p class="syasyu">タフト</p>
                  </div>
                  <p class="txt">個性的なデザインに「旅」連想させるインテリアを併せ持つ軽自動車SUVです。</p>
                </div>
              </div>
              <div class="box rank01 pc-on"><!----- Rank01 PC用 ----->
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_hustler.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts01.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">ハスラー</p>
                  </div>
                  <p class="txt">まさに「遊べる軽自動車」。アウトドアにも街乗りにもおすすめです。</p>
                </div>
              </div>
              <div class="box rank03">
                <div class="img_box">
                  <div class="img"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_gear.png" alt=""></div>
                  <div class="parts01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/ranking_parts03.png" alt=""></div>
                </div>
                <div class="txt_box">
                  <div class="tit">
                    <p class="maker">SUZUKI</p>
                    <p class="syasyu">スペーシアギア</p>
                  </div>
                  <p class="txt">SUVタイプでありながら、軽ハイトワゴンという良い所取りの1台です。</p>
                </div>
              </div>
            </div>
          </div><!------- tab-D ------->

        </div>
      </div>
    </div>
  </section>


  <section class="sec05">
    <?php require_once($setPath.'lib/include/service.php'); ?>
  </section>


  <section class="sec06">
    <div class="staff_blog">
      <div class="visual_area">
        <div class="staff">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff.png" alt="スタッフ">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/staff.png" alt="スタッフ">
        </div>
        <div class="fukidashi"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_subtit.png" alt="スタッフ情報をチェック!"></div>
        <div class="check"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_bg_txt.png" alt="CHECK"></div>
      </div>
      <div class="btn_area">
        <div class="car01"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_car01.png" alt=""></div>
        <div class="car02"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_car02.png" alt=""></div>
        <div class="car03"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_car03.png" alt=""></div>
        <div class="button"><a href="<?php home_url(); ?>/news/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_btn.png" alt=""></a></div>
      </div>
    </div>
    <div class="sns_area">
      <div class="wrapper">
        <div class="title-wrap">
          <div class="title"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/staff_sns_tit.png" alt="Dream公式SNS"></div>
        </div>

        <div class="youtube_box">
          <div class="tit_area">
            <div class="tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sns_youtube_ch.png" alt="YouTubeチャンネル"></div>
          </div>
          <div class="content">
            <div class="holder clearfix">
              <div class="box"><iframe width="100%" height="230" src="https://www.youtube.com/embed/UpSS5ptmMMY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
              <div class="box"><iframe width="100%" height="230" src="https://www.youtube.com/embed/Um7riiSFmAU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
            </div>
            <div class="button"><a href="https://www.youtube.com/channel/UCohtcDD4Fe9fym_C0PS3KsA"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sns_more_btn.png" target="_blank" alt="動画をもっと見る"></a></div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="sec07">
    <div class="wrapper">
      <div class="title_wrap01">
        <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/info_title.png" alt="お知らせ"></h2>
        <div class="en_tit"><p>NEWS</p>
        </div>

      </div>
      <div class="clearfix"><div class="more_btn"><a href="<?php home_url(); ?>/info/">more</a></div></div>
      <ul class="news_liset">
        <li>
          <?php
            $args = array(
              'category_name' => 'info', // 投稿タイプスラッグ:以下パラメータ
              'posts_per_page' => 4, // 表示する件数
            );
            $the_query = new WP_Query($args);
            ?>
          <?php if(have_posts()): ?>
          <?php while($the_query->have_posts()): $the_query->the_post(); ?>
            <a href="<?php the_permalink(); ?>" class="clearfix">
              <div class="date"><p><?php echo get_the_date('Y.n.d'); ?></p></div>
              <div class="txt">
                <p><?php echo get_the_title(); ?></p>
                <?php /*
                $p = get_post(get_the_ID());
                $content = strip_shortcodes( $p->post_content );
                echo '<p>' .wp_html_excerpt($content, 114, '...'). '</p>';
                */?>
              </div>
              <!-- <div class="txt">
                <p>お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。お知らせが⼊ります。</p>
              </div> -->
            </a>
          <?php endwhile; endif;
            wp_reset_postdata(); ?>
        </li>
      </ul>
      <div class="bnr">
        <a href="https://dreamjapan.jp/info/5711/">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_measures.png" alt="ドリームの新型コロナ感染症対策について">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_measures.png" alt="ドリームの新型コロナ感染症対策について">
        </a>
      </div>
    </div><!-- /.wrapper -->
  </section>


  <section class="sec08">
    <div class="wrapper">
      <div class="title_wrap01">
        <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/office_title.png" alt="店舗案内"></h2>
        <div class="en_tit"><p>OFFICE</p></div>
      </div>

      <div class="holder office">
        <div class="clearfix">
          <div class="box height">
            <div class="img_box">
              <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/office_kakogawa_img.png" alt="加古川本店">
            </div>
            <div class="txt_box">
              <p class="name">加古川本店</p>
              <p class="address">〒675-0053<br>兵庫県加古川市⽶⽥町船頭字⾕107-3</p>
              <div class="info_box">
                <dl>
                  <dt>TEL</dt>
                  <dd>079-434-3000</dd>
                </dl>
                <dl>
                  <dt>営業時間</dt>
                  <dd>10時～20時</dd>
                </dl>
                <dl>
                  <dt>定休⽇</dt>
                  <dd>第3水曜日</dd>
                </dl>
              </div>
            </div>
            <div class="button"><a href="<?php echo home_url(); ?>/information/#store_kakogawa">店舗詳細はこちら</a></div>
          </div>

          <div class="box height">
            <div class="img_box">
              <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/office_fukuchiyama_img.jpg" alt="福知⼭店">
            </div>
            <div class="txt_box">
              <p class="name">福知⼭店</p>
              <p class="address">〒620-0000<br>京都府福知⼭市篠尾96144</p>
              <div class="info_box">
                <dl>
                  <dt>TEL</dt>
                  <dd>0773-23-0031</dd>
                </dl>
                <dl>
                  <dt>営業時間</dt>
                  <dd>10時～20時</dd>
                </dl>
                <dl>
                  <dt>定休⽇</dt>
                  <dd>水曜日</dd>
                </dl>
              </div>
            </div>
            <div class="button"><a href="<?php echo home_url(); ?>/information/#store_fukuchiyama">店舗詳細はこちら</a></div>
          </div>

          <div class="box height">
            <div class="img_box">
              <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/office_premium_img.png" alt="ドリーム プレミアム">
            </div>
            <div class="txt_box">
              <p class="name">ドリーム プレミアム</p>
              <p class="address">〒624-0946<br>京都府舞鶴市下福井51</p>
              <div class="info_box">
                <dl>
                  <dt>TEL</dt>
                  <dd>0773-78-3399</dd>
                </dl>
                <dl>
                  <dt>営業時間</dt>
                  <dd>10時～20時</dd>
                </dl>
                <dl>
                  <dt>定休⽇</dt>
                  <dd>水曜日</dd>
                </dl>
              </div>
            </div>
            <div class="button"><a href="<?php echo home_url(); ?>/information/#store_premium">店舗詳細はこちら</a></div>
          </div>

          <div class="box height">
            <div class="img_box">
              <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/office_outlet_img.png" alt="軽アウトレット専⾨店">
            </div>
            <div class="txt_box">
              <p class="name">軽アウトレット専⾨店</p>
              <p class="address">〒620-0061<br>京都府福知⼭市荒河東町100</p>
              <div class="info_box">
                <dl>
                  <dt>TEL</dt>
                  <dd>0773-25-0398</dd>
                </dl>
                <dl>
                  <dt>営業時間</dt>
                  <dd>10時～20時</dd>
                </dl>
                <dl>
                  <dt>定休⽇</dt>
                  <dd>水曜日</dd>
                </dl>
              </div>
            </div>
            <div class="button"><a href="<?php echo home_url(); ?>/information/#store_outlet">店舗詳細はこちら</a></div>
          </div>

          <div class="box height">
            <div class="img_box">
              <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/office_kumamoto_img.jpg" alt="ドリームMEGA熊本店">
            </div>
            <div class="txt_box">
              <p class="name">ドリームMEGA熊本店</p>
              <p class="address">〒861-8035<br>熊本県熊本市東区御領8丁目10-101</p>
              <div class="info_box">
                <dl>
                  <dt>TEL</dt>
                  <dd>096-349-0100</dd>
                </dl>
                <dl>
                  <dt>営業時間</dt>
                  <dd>10時～20時</dd>
                </dl>
                <dl>
                  <dt>定休⽇</dt>
                  <dd>水曜日</dd>
                </dl>
              </div>
            </div>
            <div class="button"><a href="<?php echo home_url(); ?>/information/#store_kumamoto">店舗詳細はこちら</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="sec09">
    <div class="wrapper">
      <div class="title_wrap01">
        <h2 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/recruit_title.png" alt="採用情報"></h2>
        <div class="en_tit"><p>RECRUIT</p></div>
      </div>

      <div class="bnr_area">
        <div class="bnr01">
          <a href="https://recruit.dreamjapan.jp/" target="_blank">
            <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_recruit01.png" alt="採用情報2022">
            <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_recruit01.png" alt="採用情報2022">
          </a>
        </div>
        <div class="holder clearfix">
          <div class="bnr pc-on"><a href="https://mashgate.com/careers/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_recruit02.png" alt="DreamJapanグループ"></a></div>
          <div class="bnr pc-on"><a href="https://mechanic.dreamjapan.jp/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_recruit03.png" alt="整備⼠募集"></a></div>
          <div class="bnr sp-on"><a href="https://mashgate.com/careers/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_recruit02.png" alt="DreamJapanグループ"></a></div>
          <div class="bnr sp-on"><a href="https://mechanic.dreamjapan.jp/"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_recruit03.png" alt="整備⼠募集"></a></div>
        </div>
      </div>
    </div>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js" integrity="sha512-a/KwXZUMuN0N2aqT/nuvYp6mg1zKg8OfvovbIlh4ByLw+BJ4sDrJwQM/iSOd567gx+yS0pQixA4EnxBlHgrL6A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>
</html>
