<?php
/*
Template Name: サービス - フラット７
*/
?>


<?php
$setPath= "";
$pageTitle = "フラット７｜サービス";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css">
  </head>

  <body class="service flatseven">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">フラット７</span><br>
        <span class="en">FLAT 7</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/service/"><span itemprop="name">サービス</span></a>
        <meta itemprop="position" content="2">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">フラット７</span>
        <meta itemprop="position" content="3">
      </li>
    </ol>

    <section class="sec01">
      <div class="wrapper">
        <div class="top_txt">
          <div class="img">
            <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_title.png" alt="ドリームの登録済未使用車とは">
            <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec01_title.png" alt="ドリームの登録済未使用車とは">
          </div>
        </div>
        <ol class="point-list">
          <li class="point-list-item point01">
            <dl class=title_obi>
              <dt><img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/point_num01.png" alt="Point1">
                <img class="sp-on "src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/sp/point_num01.png" alt="Point1"></dt>
              <dd><h3>お支払いは<em>月々<br class="sp-on">定額1.1万円<small>(税込)</small></em>から！</h3></dd>
            </dl>
            <p class="txt">お支払いは月々定額です。また、年2回ボーナス時のお支払いも33,000円（税込）と低負担です。<br>
              そのため毎月の資金計画も立てやすくなります。</p>
            <div class="comparison">
              <dl class="comparison-inner">
                <dt class="comparison-title">フラット７の場合</dt>
                <dd class="comparison-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point01_pic01.png" alt="フラット７の場合"></dd>
              </dl>
              <dl class="comparison-inner">
                <dt class="comparison-title">クレジットの場合</dt>
                <dd class="comparison-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point01_pic02.png" alt="クレジットの場合"></dd>
              </dl>
            </div>
            <p class="lead">車検代や自動車税もぜんぶコミコミで月々定額1.1万円(税込)だから、<br>
              ご家庭の家計へも低負担！<br>
              月々のお給料のなかで、 無理なく車にお乗りいただけます！</p>
            <p class="pic">
              <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point01_pic03.png" alt="フラット７はこんな方にオススメ！">
              <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec01_point01_pic03.png" alt="フラット７はこんな方にオススメ！">
            </p>
          </li>
          <li class="point-list-item point02">
            <dl class=title_obi>
              <dt><img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/point_num02.png" alt="Point2">
                <img class="sp-on "src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/sp/point_num02.png" alt="Point2"></dt>
              <dd><h3><em>７年間の基本費用</em>が<br class="sp-on">全部コミコミ！</h3></dd>
            </dl>
            <p class="txt">フラット7には月額費用に車検代・税金などの維持費がほぼ全て含まれているため、 月々一定額のお支払いで、突発的に大きな金額が必要になることはほとんどありません。</p>
            <ul class="list">
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic01.png" alt="車輌代諸費用込価格"></li>
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic02.png" alt="自動車税７年間"></li>
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic03.png" alt="自賠責保険７年間"></li>
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic04.png" alt="車検代２回分"></li>
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic05.png" alt="オイル交換13回分"></li>
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic06.png" alt="フロアマット"></li>
              <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point02_pic07.png" alt="ドアバイザー"></li>
            </ul>
            <p class="lead">自動車税や自賠責保険のわずらわしいお支払いも！<br>
              全部まとめておまかせください！</p>
          </li>

          <li class="point-list-item point03">
            <dl class=title_obi>
              <dt><img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/point_num03.png" alt="Point3">
                <img class="sp-on "src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/sp/point_num03.png" alt="Point3"></dt>
              <dd><h3>豊富なオプションで<em><br class="sp-on">自分好みにカスタマイズ</em></h3></dd>
            </dl>
            <p class="txt">フルメンテナンスの安心パックや、カーナビやETCなど車をお客様の好みにカスタマイズできる単品オプション、フラット7専用の任意自動車保険を月々のお支払いに組み込むことができます。</p>
            <section class="pack">
              <div class="pack-inner">
                <h4 class="pack-title">フルサポート安心パック</h4>
                <p class="pack-txt">フルメンテナンスもついて断然お得なパックを2つご用意しました。<br>
                  お客様の必要に応じてお選びください。</p>
                <div class="plan">
                  <dl class="plan-inner">
                    <dt class="plan-title">スタンダードプラン</dt>
                    <dd class="plan-detail">
                      <div class="plan-detail-box">
                        <p class="price">月々<em>2,750</em>円<span>(税込)</span></p>
                        <ul class="detail-list">
                          <li class="detail-list-item">・タイヤ４本付</li>
                          <li class="detail-list-item">・バッテリー交換付(２回)</li>
                        </ul>
                      </div><!-- /.plan-detail-box -->
                      <p class="line"><span>フルメンテナンス</span>７年間</p>
                      <table class="table">
                        <tr>
                          <th>１２ヶ月法定点検</th>
                          <td>４回</td>
                          <th>エアクリーナー交換</th>
                          <td>１回</td>
                        </tr>
                        <tr>
                          <th>ブレーキオイル交換</th>
                          <td>２回</td>
                          <th>点火プラグ交換</th>
                          <td>１回</td>
                        </tr>
                        <tr>
                          <th>６ヶ月点検</th>
                          <td>６回</td>
                          <th>ファンベルト交換</th>
                          <td>１回</td>
                        </tr>
                        <tr>
                          <th>エンジンオイル交換</th>
                          <td>１３回</td>
                          <th>ワイパー交換</th>
                          <td>６回</td>
                        </tr>
                        <tr>
                          <th>オイルエレメント交換</th>
                          <td>６回</td>
                          <th>下廻り防錆塗装</th>
                          <td>２回</td>
                        </tr>
                        <tr>
                          <th>ブレーキパッド交換</th>
                          <td>１回</td>
                          <th>エアコンフィルター交換</th>
                          <td>２回</td>
                        </tr>
                        <tr>
                          <th>トランスミッションオイル交換</th>
                          <td>１回</td>
                          <th>ウォッシャー液補充</th>
                          <td>６回</td>
                        </tr>
                      </table>
                      <p class="note">※メンテナンスパック内容は店舗によって異なることがございます。ご了承下さい。</p>
                      <p class="text">スタンダードプラン、プレミアムプラン、どちらにも付属しているので安心！</p>
                    </dd>
                  </dl>
                  <dl class="plan-inner">
                    <dt class="plan-title">プレミアムプラン</dt>
                    <dd class="plan-detail">
                      <div class="plan-detail-box">
                        <p class="price">月々<em>2,200</em>円<span>(税込)</span></p>
                        <ul class="detail-list">
                          <li class="detail-list-item">・カーナビ</li>
                          <li class="detail-list-item">・ETC</li>
                          <li class="detail-list-item">・ガラスコーティング</li>
                        </ul>
                      </div><!-- /.plan-detail-box -->
                    </dd>
                  </dl>
                </div><!-- /.plan -->
              </div><!-- /.pack-inner -->
              <div class="pack-inner">
                <h4 class="pack-title">単品オプションでカスタマイズ</h4>
                <p class="pack-txt">色々なオプションをご用意しています。<br>
                  車をお客様の好みにカスタマイズいただけます。</p>
                <ul class="list">
                  <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_pack01.png" alt="カーナビ"></li>
                  <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_pack02.png" alt="ETC"></li>
                  <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_pack03.png" alt="CDステレオ"></li>
                  <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_pack04.png" alt="ドライブレコーダー"></li>
                  <li class="list-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_pack05.png" alt="希望ナンバー"></li>
                </ul>
              </div><!-- /.pack-inner -->
              <div class="pack-inner">
                <h4 class="pack-title">フラット７専用自動車保険</h4>
                <p class="pack-txt">さまざまなトラブルに対応したフラット7専用の自動車保険をご用意しました。<br>
                  一般的な自動車保険の車両保険では補いきれない、リースならではの保険。<br>
                  7年間の保険料も一定額です。</p>
              </div><!-- /.pack-inner -->
            </section>
          </li>
          <li class="point-list-item point04">
            <dl class=title_obi>
              <dt><img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/point_num04.png" alt="Point4">
                <img class="sp-on "src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/sp/point_num04.png" alt="Point4"></dt>
              <dd><h3>リース終了後に選べる<em>4つのプラン</em></h3></dd>
            </dl>
            <p class="txt">リース期間が満了した後は、お客様のご要望にあわせてプランをお選びいただけます。</p>
            <ul class="list">
              <li class="list-item">
                <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point04_pic01.png" alt="新しい車に乗り換える">
                <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec01_point04_pic01.png" alt="新しい車に乗り換える">
              </li>
              <li class="list-item">
                <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point04_pic02.png" alt="同じ車に乗り続ける">
                <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec01_point04_pic02.png" alt="同じ車に乗り続ける">
              </li>
              <li class="list-item">
                <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point04_pic03.png" alt="車を買い取る">
                <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec01_point04_pic03.png" alt="車を買い取る">
              </li>
              <li class="list-item">
                <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec01_point04_pic04.png" alt="車を返却する">
                <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec01_point04_pic04.png" alt="車を返却する">
              </li>
            </ul>
          </li>
        </ol>
      </div>
    </section>

    <section class="sec02">
      <div class="wrapper">
        <h3 class="section-title">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec02_ttl.png" alt="取扱車種一覧">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sp/sec02_ttl.png" alt="取扱車種一覧">
        </h3>
        <div class="type">
          <div class="type-title">
            <p class="downpay">頭金<br>
              <span>0</span><small>円</small></p>
            <p class="bonus">ボーナス時<br>
              <span>33,000</span><small>円</small></p>
            <div class="month">
              <span class="month-text">コストを抑えたい方に!!</span>
              <p class="month-inner">月々<em>8,800</em>円〜<small>(税込)</small></p>
            </div>
          </div><!-- /.tile-title -->
          <div class="slider">
            <ul class="slider-list">
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_alto_2wd.jpg" alt="スズキ アルト"></p>
                <p class="name">スズキ アルト</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>A</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>943,800円</td>
                  </tr>
                </table>
                <p class="price">月々<em>9,900</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_mira_es_2wd.jpg" alt="ダイハツ ミライース"></p>
                <p class="name">ダイハツ ミライース</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>L</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>893,200円</td>
                  </tr>
                </table>
                <p class="price">月々<em>8,800</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_every_2wd.jpg" alt="スズキ エブリィバン"></p>
                <p class="name">スズキ エブリィバン</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>PA 5MT</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>971,300円</td>
                  </tr>
                </table>
                <p class="price">月々<em>8,800</em><small>円</small></p>
              </li>
            </ul>
          </div>
        </div><!-- /.type -->

        <div class="type">
          <div class="type-title">
            <p class="downpay">頭金<br>
              <span>0</span><small>円</small></p>
            <p class="bonus">ボーナス時<br>
              <span>33,000</span><small>円</small></p>
            <div class="month">
              <span class="month-text">手頃な価格で人気車に!!</span>
              <p class="month-inner">月々<em>11,000</em>円〜<small>(税込)</small></p>
            </div>
          </div><!-- /.tile-title -->
          <div class="slider">
            <ul class="slider-list">
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_tanto_2wd.jpg" alt="ダイハツ タント"></p>
                <p class="name">ダイハツ タント</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>L<br>スマートアシストⅢ非装着車</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,243,000円</td>
                  </tr>
                </table>
                <p class="price">月々<em>11,000</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_wagon_r_2wd.jpg" alt="スズキ ワゴンR"></p>
                <p class="name">スズキ ワゴンR</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>FA<br>スズキ セーフティサポート非装着車</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,098,900円</td>
                  </tr>
                </table>
                <p class="price">月々<em>11,000</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_move_2wd.jpg" alt="ダイハツ ムーヴ"></p>
                <p class="name">ダイハツ ムーヴ</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>L</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,133,000円</td>
                  </tr>
                </table>
                <p class="price">月々<em>11,000</em><small>円</small></p>
              </li>
            </ul>
          </div>
        </div><!-- /.type -->
        <div class="type">
          <div class="type-title">
            <p class="downpay">頭金<br>
              <span>0</span><small>円</small></p>
            <p class="bonus">ボーナス時<br>
              <span>33,000</span><small>円</small></p>
            <div class="month">
              <span class="month-text">人気の車種が勢揃い!!</span>
              <p class="month-inner">月々<em>12,100</em>円〜<small>(税込)</small></p>
            </div>
          </div><!-- /.tile-title -->
          <div class="slider">
            <ul class="slider-list">
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_spacia_2wd.jpg" alt="スズキ スペーシア"></p>
                <p class="name">スズキ スペーシア</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>HYBRID G<br>スズキセーフティサポート非装着車</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,312,300円</td>
                  </tr>
                </table>
                <p class="price">月々<em>12,100</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_lapin_2wd.jpg" alt="スズキ ラパン"></p>
                <p class="name">スズキ ラパン</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>G</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,210,000円</td>
                  </tr>
                </table>
                <p class="price">月々<em>12,100</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_hustler_2wd.jpg" alt="スズキ ハスラー"></p>
                <p class="name">スズキ ハスラー</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>HYBRID G<br>スズキセーフティサポート非装着車</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,280,400円</td>
                  </tr>
                </table>
                <p class="price">月々<em>12,100</em><small>円</small></p>
              </li>
            </ul>
          </div>
        </div><!-- /.type -->
        <div class="type">
          <div class="type-title">
            <p class="downpay">頭金<br>
              <span>0</span><small>円</small></p>
            <p class="bonus">ボーナス時<br>
              <span>33,000</span><small>円</small></p>
            <div class="month">
              <span class="month-text">デザインにもこだわる方に‼</span>
              <p class="month-inner">月々<em>13,200</em>円〜<small>(税込)</small></p>
            </div>
          </div><!-- /.tile-title -->
          <div class="slider">
            <ul class="slider-list">
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_n_box_2wd.jpg" alt="ホンダ N-BOX"></p>
                <p class="name">ホンダ N-BOX</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>G</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,448,700円</td>
                  </tr>
                </table>
                <p class="price">月々<em>13,200</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_taft_2wd.jpg" alt="ダイハツ タフト"></p>
                <p class="name">ダイハツ タフト</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>X<br>スマートアシストⅢ衝突軽減ブレーキ搭載車</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,353,000円</td>
                  </tr>
                </table>
                <p class="price">月々<em>13,200</em><small>円</small></p>
              </li>
            </ul>
          </div>
        </div><!-- /.type -->
        <div class="type">
          <div class="type-title">
            <p class="downpay">頭金<br>
              <span>0</span><small>円</small></p>
            <p class="bonus">ボーナス時<br>
              <span>33,000</span><small>円</small></p>
            <div class="month">
              <span class="month-text">ちょっと贅沢に乗るなら!!</span>
              <p class="month-inner">月々<em>14,300</em>円〜<small>(税込)</small></p>
            </div>
          </div><!-- /.tile-title -->
          <div class="slider">
            <ul class="slider-list">
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_wake_2wd.jpg" alt="ダイハツ ウェイク"></p>
                <p class="name">ダイハツ ウェイク</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>D<br>スマートアシストⅢ</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,443,200円</td>
                  </tr>
                </table>
                <p class="price">月々<em>14,300</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_move_canbus_2wd.jpg" alt="ダイハツ ムーヴキャンバス"></p>
                <p class="name">ダイハツ ムーヴキャンバス</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>X<br>スマートアシストⅢ</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,430,000円</td>
                  </tr>
                </table>
                <p class="price">月々<em>14,300</em><small>円</small></p>
              </li>
            </ul>
          </div>
        </div><!-- /.type -->
        <div class="type">
          <div class="type-title">
            <p class="downpay">頭金<br>
              <span>0</span><small>円</small></p>
            <p class="bonus">ボーナス時<br>
              <span>33,000</span><small>円</small></p>
            <div class="month">
              <span class="month-text">ちょっと贅沢に乗るなら!!</span>
              <p class="month-inner">月々<em>16,500</em>円〜<small>(税込)</small></p>
            </div>
          </div><!-- /.tile-title -->
          <div class="slider">
            <ul class="slider-list">
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_tanto_custom_2wd.jpg" alt="ダイハツ タントカスタム"></p>
                <p class="name">ダイハツ タントカスタム</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>X</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,721,500円</td>
                  </tr>
                </table>
                <p class="price">月々<em>16,500</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_n_box_custom_2wd.jpg" alt="ホンダ N-BOX Custom"></p>
                <p class="name">ホンダ N-BOX Custom</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>L<br><span>Honda SENSING</span></td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,769,900円</td>
                  </tr>
                </table>
                <p class="price">月々<em>16,500</em><small>円</small></p>
              </li>
              <li class="slider-list-item">
                <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/handling_spacia_custom_2wd.jpg" alt="メーカー車種"></p>
                <p class="name">スズキ スペーシアカスタム</p>
                <table>
                  <tr>
                    <th>グレード</th>
                    <td>HYBRID GS<br>スズキセーフティーサポート装着車</td>
                  </tr>
                  <tr>
                    <th>車輌本体価格</th>
                    <td>1,663,200円</td>
                  </tr>
                </table>
                <p class="price">月々<em>16,500</em><small>円</small></p>
              </li>
            </ul>
          </div>
        </div><!-- /.type -->
        <p class="lead">ボーナスなしプランの設定や、<br class="sp-on">オプション追加も可能です。<br>
          詳しくはお問い合わせください。</p>
        <ul class="note-list">
          <li class="note-list-item">※写真はイメージです。掲載のグレードと異なる場合があります。</li>
          <li class="note-list-item">※車両本体価格はすべて税込価格です。</li>
          <li class="note-list-item">※掲載価格は2WD（5MT）の価格です。</li>
          <li class="note-list-item">※表示した数値はJC08 モード（国土交通省審査値）で定められた試験条件での値です。<br>
          　お客様の使用環境（気象、渋滞等）や運転方法（急発進、エアコン使用等）に応じて燃料消費率は異なります。</li>
        </ul>
      </div><!-- /.wrapper -->
    </section>

    <section class="sec03 faq">
      <div class="wrapper">
        <h3 class="section-title"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/service/flatseven/sec03_ttl.png" alt="よくある質問"></h3>
        <dl class="faq-inner">
          <dt class="faq-q">「リース＝借りる」というイメージに抵抗がある</dt>
          <dd class="faq-a">「分割払い」という点ではオートローンと同じです。<br>
            さらに、オートローンには含められない必要経費まで一度に分割できます。リース中は買った場合と全く同じように使用できますし、 希望ナンバーでも登録できますよ。このプランは近年増えて来ている「話題の新しい購入プラン」なんです！</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">リースだと車輌のカスタマイズはできないの？</dt>
          <dd class="faq-a">契約前ならナビやオーディオなどを付けて、分割払いに組み込めます。<br>
            契約中でも、法令に違反せず車輌の価値を下げない範囲であれば、お好みのタイヤやアルミホイールに履き替えたりすることもできます！</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">リースアップ時の手続が複雑なイメージが・・・</dt>
          <dd class="faq-a">リースアップの約3ヶ月前に、リース会社から通知が来ます。<br>
            その後の手続きは弊社に全てお任せ下さい。 期間満了後、どの選択でもリース会社との手続きを代行します！そのためお客さまに煩わしい手続きは発生しません！</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">残価の精算が心配だし、残価設定そのものに抵抗がある！</dt>
          <dd class="faq-a">7年後のお車の状況で変わります。<br>
            走行距離の基準(7万キロまで)を超えた場合やお車の内外装のキズ、へこみ、サビ等がある場合は費用等をご負担いただくことがあります。<br>
            詳しくは担当者へご確認ください。</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">リースだと割高な気がするんだけど・・・？</dt>
          <dd class="faq-a">契約期間中の税金やメンテナンス費用が含まれている為、ローンなどと比較すると割高ですが、リースは残価を設定しますのでその分が差し引かれます。そのため実際の月々は割高にはなりません。<br>
            設定残価はリースアップ時の売却予定価格ですので、フラット7では、人気車種に絞って高い残価を設定しました。<br>
            だから、安い月々払いが実現出来ているんです!</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">本当に1.1万円で乗れるのでしょうか？</dt>
          <dd class="faq-a">チラシ掲載の車は月々11,000円で多数ラインナップがあります。<br>
            車種によってはそれよりも安くなる場合がありますので、詳しくは店頭でお見積りいただければ幸いです。また、ボーナス時には33,000円(税込)が年2回必要です。</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">フラット7はどのような人が多く利用していますか？</dt>
          <dd class="faq-a">学生の方からパートや年金暮らしの方にもご好評いただいております。<br>
            審査には基準がありますが、保証人をつけるこもできますので多くの方がご利用になれます。</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">納期はどのくらい？</dt>
          <dd class="faq-a">車種によって大きくことなりますが、通常は1ヶ月半程度を予定しています。<br>
            お急ぎの場合は、1週間程度でご用意できる車もありますので店舗にてご相談ください。</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">支払方法は選べるの？</dt>
          <dd class="faq-a">ボーナス時の支払いを月々の支払いに変えることや、頭金を設定していただくこともできます。</dd>
        </dl>
        <dl class="faq-inner">
          <dt class="faq-q">オプションは選択できるの？</dt>
          <dd class="faq-a">メーカーオプションや自社のオプションを自由に選択していただくことができます。</dd>
        </dl>
      </div><!-- /.wrapper -->
    </section><!-- /.sec04 -->

    <section class="sec04">
      <div class="wrapper">
        <div class="handling">
          <h3 class="handling-title">取扱店舗</h3>
          <ul class="handling-list">
            <li class="handling-list-item">
              <p class="pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_store_maizuru01.jpg" alt="フラット７舞鶴店"></p>
              <p class="logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/handling_logo_f7.png" alt="フラット７舞鶴店"><span>舞鶴店</span></p>
              <p class="add">〒624-0946<br>
                京都府舞鶴市下福井51</p>
              <dl class="info">
                <dt>TEL</dt>
                <dd>0773-78-3399</dd>
                <dt>営業時間</dt>
                <dd>10時〜20時</dd>
                <dt>定休日</dt>
                <dd>水曜日</dd>
              </dl>
            </li>
          </ul>
        </div>
      </div><!-- /.wrapper -->
    </section><!-- /.sec04 -->

    <section>
      <?php require_once($setPath.'lib/include/service.php'); ?>
    </section>

    <?php require_once($setPath.'lib/include/footer.php'); ?>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>

      //スマホでイベントの出しわけ
      // $(window).on('spEvent',function(){
      //   var viewWidth = $(window).width();
      //   var breakPoint = 768;
      //
      //   if (viewWidth <= breakPoint) {
      //     $('.slider-list').slick({
      //       // autoplay:true,
      //       autoplaySpeed:5000,
      //       arrows: true,
      //       slidesToShow: 2,
      //     });
      //
      //   } else {
      //
      //     $('.slider-list').slick({
      //       // autoplay:true,
      //       autoplaySpeed:5000,
      //       arrows: true,
      //       slidesToShow: 3,
      //     });
      //
      //   }
      // });
      //ロードした時に実行
    	// $(window).on('load', function(){
    	// 	$(window).trigger('spEvent');
    	// });

      //slick
      $('.slider-list').slick({
        // autoplay:true,
        autoplaySpeed:5000,
        arrows: true,
        slidesToShow: 3,
        responsive : [
          {
            breakpoint: 1240,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              // centerMode: true,
            }
          },
          {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            }
          }
        ]
      });

      //よくある質問
      $('.faq-q').click(function(){
        $(this).toggleClass('is-active');
        // $(this).next('.faq-a').slideToggle(2000);
        $(this).next('.faq-a').toggleClass('is-active');
      });


    </script>
  </body>
</html>
