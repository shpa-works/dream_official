<?php
/*
Template Name: ドリームの魅力
*/
?>

<?php
$setPath= "";
$pageTitle = "ドリームの魅力";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="dream">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <div id="keyVisual">
    <h2 class="page_title">
      <span class="jp">ドリームの魅力</span><br>
      <span class="en">DREAM'S APPEAL</span>
    </h2>
  </div>

  <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
      <meta itemprop="position" content="1">
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <span itemprop="name">ドリームの魅力</span>
      <meta itemprop="position" content="2">
    </li>
  </ol>

  <section class="top_txt">
    <div class="wrapper">
      <p>ドリームは地域最大級の軽未使用車専門店です。<br>
        兵庫県・京都府・熊本県に４店舗を展開、毎月届出済未使用車を各メーカーより400〜500台仕入れているため<br>
        総在庫は常時1000台以上！<br>
        タイミングと車種のご希望が合えば、欲しかった1台が未使用車価格で買えます！</p>
    </div>
  </section>

  <section class="sec01">
    <div class="wrapper">
      <div class="title_wrap01">
        <h3 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec01_title.png" alt="ドリームは多くの実績でお客様に選ばれています！"></h3>
        <div class="en_tit"><p>SELECTED BY THE CUSTOMER</p></div>
      </div>
      <div class="holder">
        <div class="point"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec01_img01.png" alt="販売実績年間6000台"></div>
        <div class="point"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec01_img02.png" alt="車検実績年間20000台以上"></div>
        <div class="point"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec01_img03.png" alt="保険実績年間1500件"></div>
      </div>
    </div>
  </section>


  <section class="sec02">
    <div class="wrapper">
      <div class="title_wrap01">
        <h3 class="jp_tit">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_title.png" alt="ドリームは各メーカー代理店＆協力店です！">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sp/sec02_title.png" alt="ドリームは各メーカー代理店＆協力店です！">
        </h3>
        <div class="en_tit"><p>MANUFACTURE AGENCY &<br class="sp-on"> COOPERATING STORE</p></div>
      </div>
      <p class="mintit">だから、納得の品揃えと<br class="sp-on">価格を実現できます！</p>

      <div class="board">
        <div class="maker_logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_logo01.png" alt="MAZDA"></div>
        <div class="maker_logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_logo02.png" alt="MITSUBISHI MOTORS"></div>
        <div class="maker_logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_logo03.png" alt="NISSAN"></div>
        <div class="maker_logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_logo04.png" alt="TOYOTA"></div>
        <div class="maker_logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_logo05.png" alt="SUZUKI"></div>
        <div class="maker_logo"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec02_logo06.png" alt="HONDA"></div>
      </div>
    </div>
  </section>


  <section class="sec03">
    <div class="wrapper">
      <div class="title_wrap01">
        <h3 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec03_title.png" alt="最低価格保証"></h3>
        <div class="en_tit"><p>LOWEST PRICE GUARANTEE</p></div>
      </div>
      <div class="txt_cont">
        <p>軽未使用車専門店ドリームでは、最低価格保証を約束します。<br>同一グレード、同一車種の未使用車は地域で最安値を必ず守ります。<br>新車を登録しただけのお車を安く買い、オプション等の特別装備をより充実させ、<br>素敵なカーライフを送っていただける事を本気で望んでおります。<br>私達ドリームの全社員の願いは、お客様がドリームで買って本当に良かったと思ってもらえる事です。</p>
        <p class="min">※ドリームでは走行10km以内のお車に対し未使用車と呼んでいます。(100km走っていても未使用車と呼ぶ自動車販売店もあります)</p>
      </div>
      <div class="blue_box">
        <div class="title_wrap01">
          <h4 class="jp_tit"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec03_mintit.png" alt="安さの秘密"></h4>
          <div class="en_tit"><p>SECRET</p></div>
        </div>
        <ul class="imgs">
          <li><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec03_img01.png" alt="大量仕入れ＋コスト削減"></li>
          <li><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec03_img02.png" alt="大量販売＋薄利多売"></li>
          <li><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec03_img03.png" alt="システム化＋少人数"></li>
        </ul>
        <p class="txt">ご希望の車種が在庫にない場合は、希望車種をお伝えいただければ今後入庫する各メーカー在庫よりお探し致します。<br>在庫があった場合お客様名義で販売できる場合もある為、未使用車の価格で新車にお乗りいただく事が可能です。</p>
      </div>
    </div>
  </section>


  <section class="sec04">
    <div class="wrapper">
      <div class="title_wrap01">
        <h3 class="jp_tit">
          <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec04_title.png" alt="ドリームで車を買うメリット">
          <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sp/sec04_title.png" alt="ドリームで車を買うメリット">
        </h3>
        <div class="en_tit"><p>MERIT OF BUYING A CAR IN DREAM</p></div>
      </div>
      <div class="holder clearfix">
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec04_mintit01.png" alt="メリット01"></div>
          <div class="tit height02"><p>全車保証付きで最大10年間<br>アフターサービスもばっちり！</p></div>
          <p>オイル交換・点検整備・車検・保険・鈑金などすべてドリームで行います！<br>まずはドリームへ電話一本！土日祝日、お正月も休まず営業しています。</p>
        </div>
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec04_mintit02.png" alt="メリット02"></div>
          <div class="tit height02"><p>全店、<br class="sp-on">整った設備で完全サポート</p></div>
          <p>●積載車 総台数5台完備！<br>●整備・鈑金工場を完備！<br>●保険専門ブース<br>自動車保険の事ならお任せあれ！万が一の場合でも即対応！</p>
        </div>
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec04_mintit03.png" alt="メリット03"></div>
          <div class="tit height02"><p>知識豊富なクルマの<br>プロフェッショナルがご案内</p></div>
          <p>軽未使用車専門店ドリームでは、全メーカーを取り扱っているため車種の特徴を熟知したプロが、お客様の乗り方をヒヤリングし、希望に沿った車をご案内致します。各メーカーより毎年表彰されています。</p>
        </div>
        <div class="box height">
          <div class="tab"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/dream/sec04_mintit04.png" alt="メリット04"></div>
          <div class="tit height02"><p>最低価格保証</p></div>
          <p>軽未使用車専門店ドリームでは、<br>最低価格保証を約束します。</p>
        </div>
      </div>
    </div>
  </section>


  <section class="stock_bnr">
    <div class="wrapper">
      <a href="https://zaiko.dreamjapan.jp/usedcar/">
        <img class="pc-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/bnr_stock_check.png" alt="最新在庫情報をチェック！">
        <img class="sp-on" src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/index/sp/bnr_stock_check.png" alt="最新在庫情報をチェック！">
      </a>
    </div>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
</body>
</html>
