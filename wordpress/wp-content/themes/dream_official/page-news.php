<?php
/*
Template Name: 新着情報
*/
?>

<?php
$setPath= "";
$pageTitle = "新着情報";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<?php //カテゴリの読み込み
  $category = get_the_category();
  $cat = $category[0];

  //カテゴリー名
  $cat_name = $category[0]->name;

  //カテゴリーID
  $cat_id = $category[0]->cat_ID;

  //カテゴリースラッグ
  $cat_slug = $category[0]->slug;
?>
  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="category category-news">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">新着一覧</span><br>
        <span class="en">news archive</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">新着一覧</span>
        <meta itemprop="position" content="2">
      </li>
    </ol>
    <div class="rows-wrap">
      <section class="sec01 section">
        <div class="wrapper">
          <div class="section-inner">
            <h3 class="single-title title_obi">
              新着一覧
            </h3>
            <?php /*if(have_posts()): ?>
            <ol class="articles-list">
              <?php while(have_posts()):the_post(); ?>
                <li class="articles-list-item clearfix">
                  <h4 class="articles-title"><?php the_title(); ?></h4>
                  <p class="articles-date"><?php echo get_the_date('Y.n.d'); ?></p>
                  <?php
                  $p = get_post(get_the_ID());
                  $content = strip_shortcodes( $p->post_content );
                  echo '<p class="articles-excerpt">' .wp_html_excerpt($content, 114, '...'). '</p>';
                  ?>
                  <p class="articles-link"><a href="<?php the_permalink(); ?>">MORE</a></p>
                </li>
              <?php endwhile; endif; */?>
            <!-- </ol> -->
            <ol class="article-list">
              <?php
              $paged = (int) get_query_var('paged');
              $args = array(
              	'posts_per_page' => 10,
              	'paged' => $paged,
              	'orderby' => 'post_date',
              	'order' => 'DESC',
              	'post_type' => 'post',
              	'post_status' => 'publish'
              );
              $the_query = new WP_Query($args);
              if ( $the_query->have_posts() ) :
              	while ( $the_query->have_posts() ) : $the_query->the_post();
              ?>

              <li class="articles-list-item clearfix">
                <h4 class="articles-title"><?php the_title(); ?></h4>
                <p class="articles-date"><?php echo get_the_date('Y年n月j日'); ?></p>
                <p class="articles-excerpt"><?php echo mb_substr(get_the_excerpt(), 0, 60) . '...'; ?></p>
                <p class="articles-link"><a href="<?php the_permalink(); ?>">MORE</a></p>
              </li>
              <?php endwhile; endif; ?>
              </ol>
              <div class="pagenation pagenation-category">
              <?php
              if ($the_query->max_num_pages > 1) {
              	echo paginate_links(array(
              		'base' => get_pagenum_link(1) . '%_%',
              		'format' => 'page/%#%/',
              		'current' => max(1, $paged),
              		'total' => $the_query->max_num_pages,
                  'prev_text' => '<',
                  'next_text' => '>',
                  'type'    => 'list'
              	));
              }
              ?>
              </div>
              <?php wp_reset_postdata(); ?>

          </div><!-- /.section-inner -->
          <?php get_sidebar(); ?>

        </div><!-- /.wrapper -->
      </section><!-- /.sec01 -->
    </div><!-- /.rows-wrap -->
    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
