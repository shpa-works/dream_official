<?php
/*
Template Name: 最新在庫情報
*/
?>

<?php
$setPath= "";
$pageTitle = "最新在庫情報";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <?php require_once($setPath.'lib/include/head.php'); ?>
</head>

<body class="zaiko">
  <?php require_once($setPath.'lib/include/header.php'); ?>

  <div id="keyVisual">
    <h2 class="page_title">
      <span class="jp">最新在庫情報</span><br>
      <span class="en">LATEST INVENTORY INFORMATION</span>
    </h2>
  </div>

  <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
      <meta itemprop="position" content="1">
    </li>
    <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
      <span itemprop="name">最新在庫情報</span>
      <meta itemprop="position" content="2">
    </li>
  </ol>

  <section class="sec01">
    <div class="wrapper">

    <iframe type="text/html" frameborder="0" width="100%" height="2000px" src="https://zaiko.dreamjapan.jp/usedcar/usedcar_search_recommend.php" ></iframe>

    </div>
  </section>

  <?php require_once($setPath.'lib/include/footer.php'); ?>
</body>
</html>
