<?php
/*
Template Name: 店舗案内
*/
?>


<?php
$setPath= "";
$pageTitle = "店舗案内";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="information">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp">店舗案内</span><br>
        <span class="en">OFFICE INFORMATION</span>
      </h2>
    </div>

    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name">店舗案内</span>
        <meta itemprop="position" content="2">
      </li>
    </ol>

    <section class="sec01 store">
      <div class="wrapper">
        <ul class="store-list">
          <li class="store-list-item" id="store_kakogawa">
            <p class="store-list-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/store_pic01.jpg" alt="ドリーム加古川本店"></p>
            <div class="store-list-inner">
              <h3 class="store-list-title">
                ドリーム加古川本店
                <span>
                  <a href="https://www.instagram.com/dream_kuruma/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                  <a href="https://www.facebook.com/112343473457046/" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
                </span>

              </h3>
              <p class="add">〒675-0053<br>兵庫県加古川市米田町船頭字谷107-3</p>
              <div class="info">
                <dl class="inner clearfix">
                  <dt class="inner-title">TEL</dt>
                  <dd class="inner-item">079-434-3000</dd>
                  <dt class="inner-title">営業時間</dt>
                  <dd class="inner-item">10時～20時</dd>
                  <dt class="inner-title">定休日</dt>
                  <dd class="inner-item">第3水曜日</dd>
                </dl>

                <ul class="detail">
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sale.png" alt="車輌販売"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_inspection.png" alt="車検"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sheetmetal.png" alt="鈑金"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_insurance.png" alt="保険"></li>
                </ul>
              </div><!-- /.detail -->
            </div><!-- /.store-list-inner -->
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3277.1436038988845!2d134.82851331485767!3d34.77715998041498!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3554d8752211e46b%3A0xf3d0eafb83294de4!2z44OJ44Oq44O844OgIOWKoOWPpOW3neacrOW6lw!5e0!3m2!1sja!2sjp!4v1643819371516!5m2!1sja!2sjp" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <p class="link">
              <a href="<?php home_url(); ?>/deal/#chirashi_kakogawa">
                <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/button_link.png" alt="この店舗のお得な情報はこちら"></p>
              </a>
          </li>
          <li class="store-list-item" id="store_fukuchiyama">
            <p class="store-list-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/store_pic02.jpg" alt="ドリーム福知山店"></p>
            <div class="store-list-inner">
              <h3 class="store-list-title">
                ドリーム福知山店
                <span>
                  <a href="https://www.instagram.com/dreamfukuchiyama/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                  <a href="https://www.facebook.com/103669361079733/" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
                </span>
              </h3>
              <p class="add">〒620-0000<br>京都府福知⼭市篠尾96144</p>
              <div class="info">
                <dl class="inner clearfix">
                  <dt class="inner-title">TEL</dt>
                  <dd class="inner-item">0773-23-0031</dd>
                  <dt class="inner-title">営業時間</dt>
                  <dd class="inner-item">10時～20時</dd>
                  <dt class="inner-title">定休日</dt>
                  <dd class="inner-item">⽔曜⽇</dd>
                </dl>

                <ul class="detail">
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sale.png" alt="車輌販売"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_inspection.png" alt="車検"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_insurance.png" alt="保険"></li>
                </ul>
              </div><!-- /.detail -->
            </div><!-- /.store-list-inner -->
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13025.28820285117!2d135.10131576977537!3d35.2979785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000025fce155555%3A0x8cbd5cd6c30feeaa!2z44OJ44Oq44O844OgIOemj-efpeWxseW6lw!5e0!3m2!1sja!2sjp!4v1643888025264!5m2!1sja!2sjp" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <p class="link">
              <a href="<?php home_url(); ?>/deal/#chirashi_fukuchiyama">
                <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/button_link.png" alt="この店舗のお得な情報はこちら">
              </a>
            </p>
          </li>
          <li class="store-list-item" id="store_premium">
            <p class="store-list-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/store_pic03.jpg" alt="ドリームプレミアム"></p>
            <div class="store-list-inner">
              <h3 class="store-list-title">
                ドリーム プレミアム
                <span>
                  <a href="https://www.instagram.com/flat7_dreammaizuru/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                  <a href="https://www.facebook.com/dream.flat7/" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
                </span>
              </h3>
              <p class="add">〒624-0946<br>京都府舞鶴市下福井51</p>
              <div class="info">
                <dl class="inner clearfix">
                  <dt class="inner-title">TEL</dt>
                  <dd class="inner-item">0773-78-3399</dd>
                  <dt class="inner-title">営業時間</dt>
                  <dd class="inner-item">10時～20時</dd>
                  <dt class="inner-title">定休日</dt>
                  <dd class="inner-item">水曜日</dd>
                </dl>

                <ul class="detail">
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sale.png" alt="車輌販売"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_inspection.png" alt="車検"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sheetmetal.png" alt="鈑金"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_rentacar.png" alt="レンタカー"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_insurance.png" alt="保険"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_flat7.png" alt="フラット７"></li>
                </ul>
              </div><!-- /.detail -->
            </div><!-- /.store-list-inner -->
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3250.2535926280657!2d135.30814695092792!3d35.44851578015227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5fff8bb4756822cd%3A0x72f58fc8f32a640a!2z44OJ44Oq44O844Og6Iie6ba05bqX!5e0!3m2!1sja!2sjp!4v1470815525729" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <p class="link">
              <a href="<?php home_url(); ?>/deal/#chirashi_premium">
                <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/button_link.png" alt="この店舗のお得な情報はこちら"></p>
              </a>
          </li>
          <li class="store-list-item" id="store_outlet">
            <p class="store-list-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/store_pic04.jpg" alt="軽アウトレット専門店"></p>
            <div class="store-list-inner">
              <h3 class="store-list-title">
                軽アウトレット専門店
                <span>
                  <a href="https://www.instagram.com/flat7_dreammaizuru/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                  <a href="https://www.facebook.com/804368223249450/" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a>
                </span>
              </h3>
              <p class="add">〒620-0061<br>京都府福知⼭市荒河東町100</p>
              <div class="info">
                <dl class="inner clearfix">
                  <dt class="inner-title">TEL</dt>
                  <dd class="inner-item">0773-25-0398</dd>
                  <dt class="inner-title">営業時間</dt>
                  <dd class="inner-item">10時～20時</dd>
                  <dt class="inner-title">定休日</dt>
                  <dd class="inner-item">水曜日</dd>
                </dl>

                <ul class="detail">
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sale.png" alt="車輌販売"></li>
                </ul>
              </div><!-- /.detail -->
            </div><!-- /.store-list-inner -->
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3255.7469469111925!2d135.11018135032305!3d35.31226875762994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60000344934eb401%3A0xdb860b37f6f70f42!2z6Lu944OJ44Oq44O844Og77yv77y177y077ys77yl77y0!5e0!3m2!1sja!2sjp!4v1643890622048!5m2!1sja!2sjp" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <p class="link">
              <a href="<?php home_url(); ?>/deal/#chirashi_outlet">
                <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/button_link.png" alt="この店舗のお得な情報はこちら"></p>
              </a>
          </li>
          <li class="store-list-item" id="store_kumamoto">
            <p class="store-list-pic"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/store_pic05.jpg" alt="ドリームMEGA熊本店"></p>
            <div class="store-list-inner">
              <h3 class="store-list-title">
                ドリームMEGA熊本店
                <span>
                  <a href="https://www.instagram.com/dream_kumamoto/" class="insta" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_insta.png" alt="instagram"></a>
                  <!-- <a href="" class="fb" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/common/icon_fb.png" alt="Facebook"></a> -->
                </span>
              </h3>
              <p class="add">〒861-8035<br>熊本県熊本市東区御領8丁目10-101</p>
              <div class="info">
                <dl class="inner clearfix">
                  <dt class="inner-title">TEL</dt>
                  <dd class="inner-item">096-349-0100</dd>
                  <dt class="inner-title">営業時間</dt>
                  <dd class="inner-item">10時～20時</dd>
                  <dt class="inner-title">定休日</dt>
                  <dd class="inner-item">水曜日</dd>
                </dl>
                <ul class="detail">
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_sale.png" alt="車輌販売"></li>
                  <li class="detail-item"><img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/icon_insurance.png" alt="保険"></li>
                </ul>
              </div><!-- /.detail -->
            </div><!-- /.store-list-inner -->
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d214553.78430413836!2d130.775712!3d32.834583!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3626d9c0d05c3666!2z44OJ44Oq44O844OgIE1FR0HnhormnKzlupc!5e0!3m2!1sja!2sjp!4v1644411514604!5m2!1sja!2sjp" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <p class="link">
              <a href="<?php home_url(); ?>/deal/#chirashi_kumamoto">
                <img src="<?php echo get_template_directory_uri(); ?>/lib/cmn-img/information/button_link.png" alt="この店舗のお得な情報はこちら"></p>
              </a>
          </li>
        </ul>
      </div><!-- /.wrapper -->
    </section><!-- /.sec01 -->

    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
