
<?php
$setPath= "";
$pageTitle = "新着情報";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<?php //カテゴリの読み込み
  $category = get_the_category();
  $cat = $category[0];

  //カテゴリー名
  $cat_name = $cat->name;

  //カテゴリーID
  $cat_id = $cat->cat_ID;

  //カテゴリースラッグ
  $cat_slug = $cat->slug;
?>
  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="category category-<?php echo $cat_slug; ?> post">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp"><?php echo $cat_name; ?></span><br>
        <span class="en"><?php echo strtoupper($cat_slug); ?></span>
      </h2>
    </div>


    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/news/"><?php echo $cat_name; ?></a>
        <meta itemprop="position" content="2">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name"><?php the_title(); ?></span>
        <meta itemprop="position" content="3">
      </li>
    </ol>
    <div class="rows-wrap">
      <section class="sec01 section">
        <div class="wrapper">
          <div class="section-inner">
            <?php if(have_posts()): ?>

            <?php while(have_posts()):the_post(); ?>
              <h3 class="single-title title_obi">
                <?php the_title(); ?>
              </h3>
              <div class="single-content">
                <p class="date"><?php echo get_the_date('Y.n.d'); ?></p>
                <?php the_content(); ?>
              </div><!-- /.single-content -->
            <?php endwhile; endif; ?>

            <div class="pagenation pagenation-post">
              <?php // 現在の投稿に隣接している前後の投稿を取得する
                $prev_post = get_previous_post(); // 前の投稿を取得
                $next_post = get_next_post(); // 次の投稿を取得
                if( $prev_post || $next_post ): // どちらか一方があれば表示
              ?>

              <p class="button button-prev">
                <?php if( $prev_post ): // 前の投稿があれば表示 ?>
                  <a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="prev-link">
                    前の記事
                  </a>
                <?php endif; ?>
              </p>
              <p class="button button-parent">

                <a href="<?php echo home_url() . "/" . $cat_slug; ?>"><?php echo $cat_name; ?>TOP</a>
              </p>

              <p class="button button-next">
                <?php if( $next_post ): // 次の投稿があれば表示 ?>
                  <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="next-link">
                    次の記事
                  </a>
                <?php endif; ?>
              </p>
              <?php endif; ?>
            </div><!-- /.pagenation -->
          </div><!-- /.section-inner -->
          <?php get_sidebar(); ?>
        </div><!-- /.wrapper -->
      </section><!-- /.sec01 -->
    </div><!-- /.rows-wrap -->
    <section>
      <?php require_once($setPath.'lib/include/service.php'); ?>
    </section>
    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
