
<?php
$setPath= "";
$pageTitle = "新着情報";
$pageInfo = array(
  "title" => $pageTitle,
  "keywords" => "",
  "description" => "",
);
?>
<!DOCTYPE html>
<html lang="ja">

<?php //カテゴリの読み込み
  $category = get_the_category();
  $cat = $category[0];

  //カテゴリー名
  $cat_name = $category[0]->name;

  //カテゴリーID
  $cat_id = $category[0]->cat_ID;

  //カテゴリースラッグ
  $cat_slug = $category[0]->slug;
?>
  <head>
    <?php require_once($setPath.'lib/include/head.php'); ?>
  </head>

  <body class="category category-<?php echo $cat_slug; ?>">
    <?php require_once($setPath.'lib/include/header.php'); ?>

    <div id="keyVisual">
      <h2 class="page_title">
        <span class="jp"><?php get_the_archive_title(); ?></span><br>
        <span class="en">archive</span>
      </h2>
    </div>


    <ol id="breadcrumbs" class="wrapper" itemscope="" itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name">トップ</span></a>
        <meta itemprop="position" content="1">
      </li>
      <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span itemprop="name"><?php echo $cat_name; ?></span>
        <meta itemprop="position" content="2">
      </li>
    </ol>
    <div class="rows-wrap">
      <section class="sec01 section">
        <div class="wrapper">
          <div class="section-inner">
            <h3 class="single-title title_obi">
              <?php echo $cat_name; ?>一覧
            </h3>
            <?php if(have_posts()): ?>
            <ol class="articles-list">
              <?php while(have_posts()):the_post(); ?>
                <li class="articles-list-item clearfix">
                  <h4 class="articles-title"><?php the_title(); ?></h4>
                  <p class="articles-date"><?php echo get_the_date('Y.n.d'); ?></p>
                  <?php
                  $p = get_post(get_the_ID());
                  $content = strip_shortcodes( $p->post_content );
                  echo '<p class="articles-excerpt">' .wp_html_excerpt($content, 114, '...'). '</p>';
                  ?>
                  <p class="articles-link"><a href="<?php the_permalink(); ?>">MORE</a></p>
                </li>
              <?php endwhile; endif; ?>
            </ol>

            <div class="pagenation pagenation-category">
              <?php //the_category_pagination(
                // array (
                //   'mid_size'      => 10, // 現在ページの左右に表示するページ番号の数
                //   'prev_next'     => true,
                //   'prev_text'     => __( '<'),
                //   'next_text'     => __( '>'),
                //   'type'          => 'list',
                //   )
                //); ?>
            </div><!-- /.pagenation -->
          </div><!-- /.section-inner -->
          <?php get_sidebar(); ?>

        </div><!-- /.wrapper -->
      </section><!-- /.sec01 -->
    </div><!-- /.rows-wrap -->
    <?php require_once($setPath.'lib/include/footer.php'); ?>
  </body>
</html>
